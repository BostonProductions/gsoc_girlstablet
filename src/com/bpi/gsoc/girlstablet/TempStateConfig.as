package com.bpi.gsoc.girlstablet
{
	import com.bpi.citrusas.ui.primitives.Primitive_Rectangle;
	import com.bpi.citrusas.utils.Utils_BPI;
	import com.bpi.citrusas.utils.Utils_Math;
	import com.bpi.gsoc.framework.avatar.HairstyleReference;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_DB;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_JC;
	import com.bpi.gsoc.framework.career.keys.CareerKeys_SA;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.Issue_Data;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.embeddedcontent.EmbeddedFonts;
	import com.bpi.gsoc.framework.states.takeaction.Issue;
	import com.bpi.gsoc.framework.states.takeaction.IssueReference;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_AssembleYourTeam;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_BrainStorm;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ChooseIssue;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_CommunityResources;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_KnowYourRole;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_MindMap;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.geom.Point;
	
	import mx.utils.UIDUtil;

	public class TempStateConfig
	{
		
		// ****** CAREER ********//
		
		// PICTURE YOURSELF
//				public static const State:int = StateController_IDs.STATECONTROLLER_PICTUREYOURSELF;
//				public static const Screen:int = StateController_StateIDs_PictureYourself.STATE_NAMESELECTION;
		//		public static const Screen:int = StateController_StateIDs_PictureYourself.STATE_TAKEPICTURE;
//				public static const Screen:int = StateController_StateIDs_PictureYourself.STATE_CUSTOMIZECHARACTER;
		
		// EXPLORE YOUR PASSION
		//		public static const State:int = StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION;
		//		public static const Screen:int = StateController_StateIDs_ExploreYourPassion.STATE_MULTIPLEQUESTIONS;
		//		public static const Screen:int = StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE;
		
		// CHOOSE YOUR CAREER
		//		public static const State:int = StateController_IDs.STATECONTROLLER_CHOOSEYOURCAREER;
		//		public static const Screen:int = StateController_StateIDs_ChooseYourCareer.STATE_CAREERCHOICE;
		//		public static const Screen:int = StateController_StateIDs_ChooseYourCareer.STATE_CAREERDONE;
		
		// KNOW YOUR ROLE
//				public static const State:int = StateController_IDs.STATECONTROLLER_KNOWYOURROLE;
//				public static const Screen:int = StateController_StateIDs_KnowYourRole.STATE_CHOICEWEB;
		
		// **********************//
		
		
		
		// ****** TAKE ACTION ********//
		
		// ASSEMBLE YOUR TEAM
//				public static const State:int = StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM;
//				public static const Screen:int = StateController_StateIDs_AssembleYourTeam.STATE_NAMESELECTION;
		//		public static const Screen:int = StateController_StateIDs_AssembleYourTeam.STATE_TAKEPICTURE;
		//		public static const Screen:int = StateController_StateIDs_AssembleYourTeam.STATE_CUSTOMIZECHARACTER;
//				public static const Screen:int = StateController_StateIDs_AssembleYourTeam.STATE_CHOOSETEAMNAME;
		
		// CHOOSE ISSUE
//				public static const State:int = StateController_IDs.STATECONTROLLER_CHOOSEISSUE;
//				public static const Screen:int = StateController_StateIDs_ChooseIssue.STATE_CHOOSEISSUE;
		
		// COMMUNITY RESOURCES
//				public static const State:int = StateController_IDs.STATECONTROLLER_COMMUNITYRESOURCES;
//				public static const Screen:int = StateController_StateIDs_CommunityResources.STATE_COMMUNITYRESOURCES;
		
		// MIND MAP
//				public static const State:int = StateController_IDs.STATECONTROLLER_MINDMAP;
//				public static const Screen:int = StateController_StateIDs_MindMap.STATE_MINDMAP;
		
		// BRAINSTORM
				public static const State:int = StateController_IDs.STATECONTROLLER_BRAINSTORM;
//				public static const Screen:int = StateController_StateIDs_BrainStorm.STATE_BRAINSTORM;
				public static const Screen:int = StateController_StateIDs_BrainStorm.STATE_PROJECTGOALS;
		
		// PROJECT PLANNING
//				public static const State:int = StateController_IDs.STATECONTROLLER_PROJECTPLANNING;
//				public static const Screen:int = StateController_StateIDs_ProjectPlanning.STATE_MAIN;
		
		// **********************//

		// RECORD VIDEO
//		public static const State:int = StateController_IDs.STATECONTROLLER_RECORDVIDEO;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_WRITESCRIPT;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_WARMUP;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_RECORDVIDEO;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_REVIEWVIDEO;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_EDITVIDEO;
//		public static const Screen:int = StateController_StateIDs_RecordVideo.STATE_PLAYBACKVIDEO;
		
		// POSTER
//		public static const State:int = StateController_IDs.STATECONTROLLER_POSTER;
//		public static const Screen:int = StateController_StateIDs_Poster.STATE_POSTER;
		
		
		/**
		 * This creates a test set of girls.
		 */
		public static function InitializeDebugState(numScouts:int = 5):void
		{
			for(var index:int = 0; index < numScouts; index++)
			{
				var uID:String = UIDUtil.createUID();
				var girlUserData:GSOC_UserData = new GSOC_UserData();
				girlUserData.firstName = Utils_BPI.GenerateRandomString(5);
				girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_HAIR_STYLE, int(Utils_Math.RandomInt(0, HairstyleReference.GetNumberOfHairstyles() - 1)));
				girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_HAIR_COLOR, uint(Utils_Math.RandomInt(0, 16581375)));
				girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_SKIN_COLOR, uint(Utils_Math.RandomInt(0, 16581375)));
				var issue:Issue = IssueReference.GetIssuesByAgeAndJourney(SessionInformation_GirlsTablet.Age, SessionInformation_GirlsTablet.Journey)[0];
				var issueData:Issue_Data = SessionInformation_GirlsTablet.TeamInfo.Team_Issue;
				issueData.Key = issue.Key;
				issueData.Name = issue.IssueName;
				
				for(var i:int = 0; i < 2; i++)
				{
					var sprite:Primitive_Rectangle = new Primitive_Rectangle(new Point(Math.random() * 1000, Math.random() * 1000));
					sprite.Color = Math.random()*0xffffff;
					SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetImage(Utils_BPI.GenerateRandomString(20), sprite); 
				}

				switch(SessionInformation_GirlsTablet.Age)
				{
					case AgeConstants.KEY_DAISYBROWNIE:
						girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE, CareerKeys_DB.ACTRESS);
						break;
					case AgeConstants.KEY_JUNIORCADETTE:
						girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE, CareerKeys_JC.SCHOOLPRINCIPAL);
						break;
					case AgeConstants.KEY_SENIORAMBASSADOR:
						girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_CAREER_TITLE, CareerKeys_SA.BUILDINGPROJECTMANAGER);
						break;
				}
				
				var mcAnswers:Array = new Array(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1); // mcAnswers[index] - index is question num. value is career weight
				for (var j:int = 0; j < 12; j++)
				{
					mcAnswers[j] = Utils_Math.RandomInt(0, 2);
				}
				
				girlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY, mcAnswers);
				SessionInformation_GirlsTablet.TeamInfo.AddTeamMember(uID, "Girl_" + index);
				SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationFromUID(uID).GirlUserData = girlUserData;
				
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_COMMUNITYRESOURCES_BUILDING_LIST, "????");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_TEAM_NAME, "The Debugging Squad");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_CREDITS, "Director: Random Name\n Editor: Random name\n Actor: Random Name\n");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER, "who knows what goes here yet");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FONT, EmbeddedFonts.FontName_ZalderdashTTF);
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_SONG, "data/song1.mp3");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_TITLE, "Debug Video Title");
				SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_USECREDITS, true);
			}
		}
	}
}
package com.bpi.gsoc.girlstablet.global_data
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.greensock.TweenLite;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;

	public class KeyboardLauncher
	{
		private static const TAB_TIP_COMMAND:Vector.<String> = new <String>["start C:/PROGRA~1/COMMON~1/MICROS~1/ink/TabTip.exe"];
		private static const COMMAND_PROMPT:File = new File("c:\\windows\\system32\\cmd.exe");
		private static var m_NativeProcess:NativeProcess;
		private static var m_NativeProcessStartupInfo:NativeProcessStartupInfo;
		public static var KeyboardLauncherEventDispatcher:EventDispatcher
		
		public static function Init():void
		{
			KeyboardLauncherEventDispatcher = new EventDispatcher();
		}
		public static function LaunchWindowsKeyboard():void
		{
			m_NativeProcessStartupInfo = new NativeProcessStartupInfo(); 
			m_NativeProcessStartupInfo.executable = COMMAND_PROMPT;             

			m_NativeProcess = new NativeProcess();                
			m_NativeProcess.start(m_NativeProcessStartupInfo);
			m_NativeProcess.standardInput.writeUTFBytes(TAB_TIP_COMMAND + "\n"); 
			
			launchModalTextInput();
			
			CitrusFramework.frameworkStage.addEventListener(Event.DEACTIVATE, report);
			TweenLite.delayedCall(0.5, m_NativeProcess.exit); // kill the command prompt after the soft keyboard is launched
		}
		
		private static function launchModalTextInput():void
		{
			CitrusFramework.frameworkStage.addChild(SoftKeyboardInput.getInstance());
			SoftKeyboardInput.getInstance().LaunchWindow();
		}
		
		private static function closeModalTextInput(e:Event = null):void
		{
			SoftKeyboardInput.getInstance().ExitWindow(removeModalTextInput);	
		}
		
		private static function removeModalTextInput(e:Event = null):void
		{
			CitrusFramework.frameworkStage.removeChild(SoftKeyboardInput.getInstance());
		}
		
		private static function report(e:Event):void
		{
			trace("stage was deactivated");
		}
	}
}
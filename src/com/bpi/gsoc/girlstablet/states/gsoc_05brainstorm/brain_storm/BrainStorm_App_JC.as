package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm
{
	import com.bpi.gsoc.components.brainstorm.BrainStorm_JC_Skin;
	import com.bpi.gsoc.components.brainstorm.IconBox_JC_Skin;
	import com.bpi.gsoc.components.global.balanceSkill;
	import com.bpi.gsoc.components.global.beakerSkill;
	import com.bpi.gsoc.components.global.calendarSkill;
	import com.bpi.gsoc.components.global.clipboardSkill;
	import com.bpi.gsoc.components.global.compsoundSkill;
	import com.bpi.gsoc.components.global.muscleSkill;
	import com.bpi.gsoc.components.global.paintsSkill;
	import com.bpi.gsoc.components.global.quotesSkill;
	import com.bpi.gsoc.components.global.sneakerSkill;
	import com.bpi.gsoc.components.global.trophySkill;
	import com.bpi.gsoc.components.global.weatherSkill;
	import com.bpi.gsoc.components.global.writingSkill;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup.Drawing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.selections_popup.BasicSelection_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.typing_popup.Typing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.BasicSticky;
	
	import flash.display.Bitmap;

	public class BrainStorm_App_JC extends BrainStorm_App
	{
		private var m_TypedSkin:BrainStorm_JC_Skin;
		
		public function BrainStorm_App_JC()
		{
			m_Skin = new BrainStorm_JC_Skin();
			m_TypedSkin = (m_Skin as BrainStorm_JC_Skin);
		}
		
		public override function Initialize():void
		{
			var iconsPopup:Icons_Popup = new Icons_Popup(m_TypedSkin.Icons_Popup);
			m_PopupManager.AddPopup(new BasicSelection_Popup(m_TypedSkin.Selection_Popup));
			m_PopupManager.AddPopup(new Drawing_Popup(m_TypedSkin.Drawing_Popup));
			m_PopupManager.AddPopup(iconsPopup);
			m_PopupManager.AddPopup(new Typing_Popup(m_TypedSkin.Typing_Popup));
			
			m_SubmitButton = m_TypedSkin.submit;
			m_IssueCause = m_TypedSkin.issuecause;
			
			iconsPopup.Background = IconBox_JC_Skin;
			iconsPopup.AddSkillIcon(new Bitmap(new writingSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new weatherSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new trophySkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new quotesSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new sneakerSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new paintsSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new muscleSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new compsoundSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new calendarSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new beakerSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new balanceSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new clipboardSkill()));
			
			var displayColor:uint = GSOC_Info.ColorTwo;
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote1, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote2, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote3, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote4, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote5, displayColor));
			m_StickyNoteManager.AddStickyNote(new BasicSticky(m_TypedSkin.stickynote6, displayColor));
			
			m_StickyNoteManager.SetupAssets(m_TypedSkin.addyourown, m_TypedSkin.garbagebin);
			
			super.Initialize();
			
			//this.addChild(new HeaderArrows(1800, -80));
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
	}
}
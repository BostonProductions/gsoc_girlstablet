package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup
{
	import com.bpi.citrusas.events.CourierEvent;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.Popup_IDs;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;

	public class Drawing_Popup extends BrainStormPopup
	{
		private var m_DrawingArea:DrawingArea;
		private var m_ColorSelection:ColorPicker;
		
		private var m_HasExisted:Boolean;
		
		public function Drawing_Popup(skin:MovieClip)
		{
			m_DrawingArea = new DrawingArea(skin.stickynote_draw.DisplayArea);
			m_DrawingArea.m_EventDispatcher.addEventListener(m_DrawingArea.m_DrawEvent, eh_DrawingOccurred);
			m_ColorSelection = new ColorPicker(skin.colorselector, skin, "COLOR_CHANGED");
			
			(skin.stickynote_draw.DisplayArea.textbox as TextField).selectable = false;
			(skin.stickynote_draw.DisplayArea.textbox as TextField).mouseEnabled = false;
			
			m_CurrentPopupID = Popup_IDs.DRAWING_POPUP;
			super(skin);
		}
		
		public override function DisplayPopUp():void
		{
			m_DrawingArea.Initialize();
			m_ColorSelection.addEventListener(m_ColorSelection.getColorChangeEventName(), eh_ColorChanged);
			
			if(CurrentStickyNote.DisplayArea.CurrentDrawing)
			{
				m_DrawingArea.ExistingDrawing = Utils_Image.ConvertDisplayObjectToBitmap(CurrentStickyNote.DisplayArea.CurrentDrawing);
				m_HasExisted = true;
				m_DrawingArea.HideInstructions(true);
				if(m_SaveButton) m_SaveButton.visible = true;
			}
			else
			{
				m_HasExisted = false;
				if(m_SaveButton) m_SaveButton.visible = false;
			}
			
			super.DisplayPopUp();
		}
		
		protected override function eh_ClosePopup(e:Event):void
		{
			super.eh_ClosePopup(e);
			
			m_DrawingArea.Clear();
			
			m_DrawingArea.DeInitialize();
			m_ColorSelection.removeEventListener(m_ColorSelection.getColorChangeEventName(), eh_ColorChanged);
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			m_DrawingArea.Clear();
			
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			m_DrawingArea.HideInstructions(true);
			
			super.eh_SaveButton_Pressed(e);
			
			m_DrawingArea.Clear();
		}
		
		private function eh_DrawingOccurred(e:Event):void
		{
			if(m_SaveButton) m_SaveButton.visible = true;
		}
		
		private function eh_ColorChanged(e:CourierEvent):void
		{
			m_DrawingArea.Color = e.payload[3];
		}
		
		public function get Drawing():Bitmap
		{
			return m_DrawingArea.Drawing;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.constants.Keys_FileNames;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_ImageKeys;
	import com.bpi.gsoc.framework.gsocui.SkillMap.Events.Event_MapStatus;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.upload.UploadManager;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.BrainStorm_App;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.geom.Point;

	public class StickyNoteManager
	{
		private static var m_Instance:StickyNoteManager;
		private var m_StickyNotes:Vector.<BasicSticky>;
		private var m_StickyNotePositions:Vector.<Point>;
		
		private var m_AddYourOwnButton:AddYourOwnSticky;
		private var m_TrashCan:MovieClip;
		
		public function StickyNoteManager()
		{
			if(m_Instance == null)
			{
				m_Instance = this;
			}
			
			m_StickyNotes = new Vector.<BasicSticky>();
			m_StickyNotePositions = new Vector.<Point>();
			m_AddYourOwnButton = new AddYourOwnSticky();
		}
		
		public function Initialize():void
		{
			
		}
		
		public function DeInitialize():void
		{
			m_AddYourOwnButton.DeInitialize();
			
			for(var i:int = 0; i < m_StickyNotes.length; i++)
			{
				m_StickyNotes[i].MoveStickyNoteTo(m_StickyNotePositions[i]);
				m_StickyNotes[i].DeInitialize();
			}
			m_Instance = null;
		}
		
		public function SetupAssets(addYourOwnButton:MovieClip, trashCan:MovieClip):void
		{
			m_AddYourOwnButton.SetupAssets(addYourOwnButton);
			m_TrashCan = trashCan;
		}
		
		public function AddStickyNote(stickyNote:BasicSticky):void
		{
			m_StickyNotes.push(stickyNote);
			m_StickyNotePositions.push(stickyNote.CurrentPosition);

			stickyNote.Initialize();
			stickyNote.Hide();
		}
		
		public function clearStickyNote(stickyNote:BasicSticky):void
		{
			stickyNote.Hide();
			stickyNote.DisplayArea.RemoveItems();
			
			m_AddYourOwnButton.RePositionAddStickyButton();
			checkBoardStatus();
		}
		
		public function SaveStickyNote(stickyNote:BasicSticky, popup:BrainStormPopup):void
		{
			stickyNote.Display();
			stickyNote.SaveInfo(popup);
			
			m_AddYourOwnButton.RePositionAddStickyButton();
			checkBoardStatus();
			
			submitData();
		}
		
		private function submitData():void
		{		
			(BrainStorm_App.Instance.skin.posterarea as MovieClip).gotoAndStop(SessionInformation_GirlsTablet.Age + 1);
			var mc:MovieClip = Utils_Image.ConvertDisplayObjectToMovieClip(BrainStorm_App.Instance.skin.posterarea);
			
			for(var i:int = 0; i < m_StickyNotes.length; i++)
			{
				if(m_StickyNotes[i].IsActive)
				{
					var bm:Bitmap = Utils_Image.GetNonTransparentAreaOfDisplayObject(Utils_Image.ConvertDisplayObjectToBitmap(m_StickyNotes[i].Skin));
					
					bm.x = m_StickyNotes[i].CurrentPosition.x - BrainStorm_App.Instance.skin.posterarea.x - (m_StickyNotes[i].Skin.width * (1/m_StickyNotes[i].Skin.scaleX)) * .5;
					bm.y = m_StickyNotes[i].CurrentPosition.y - BrainStorm_App.Instance.skin.posterarea.y - (m_StickyNotes[i].Skin.height * (1/m_StickyNotes[i].Skin.scaleY)) * .5;

					mc.addChild(bm);
				}
			}
			
			UploadManager.UploadDisplayObject(mc, Keys_FileNames.KEY_BRAINSTORM_BOARD);
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetImage(GSOC_UserData_Team_ImageKeys.KEY_BRAINSTORM_BOARD, mc);
		}
		
		public function RemoveLast():void
		{
			CitrusFramework.frameworkStage.removeChild(CitrusFramework.frameworkStage.getChildAt(CitrusFramework.frameworkStage.numChildren - 1));
		}
		
		public function StickyNoteMoved(stickyNote:BasicSticky, position:Point):void
		{
			var closestPosition:Point = GetClosestPosition(position);
			
			if(Point.distance(new Point(m_TrashCan.x, m_TrashCan.y), position) < Point.distance(closestPosition, position) && !(stickyNote is AddYourOwnSticky))
			{
				throwAwayStickyNote(stickyNote as BasicSticky);
			}
			else
			{
				var targetStickyNote:BasicSticky = GetStickyNoteByPosition(closestPosition);
				swapStickyNotes(stickyNote, targetStickyNote);
				m_AddYourOwnButton.RePositionAddStickyButton();
			}
			
			submitData();
		}
		
		public function GetClosestPosition(position:Point):Point
		{
			var closestDistance:Number = 99999;
			var closestPosition:Point;
			var positions:Vector.<Point> = StickyNoteManager.Instance.StickyNoteLocations;
			
			for(var i:int = 0; i < positions.length; i++)
			{
				var distance:Number = Point.distance(positions[i], position);
				
				if(distance < closestDistance)
				{
					closestDistance = distance;
					closestPosition = positions[i];
				}
			}
			
			return closestPosition;
		}
		
		private function swapStickyNotes(sticky1:BasicSticky, sticky2:BasicSticky):Boolean
		{
			var shouldSwap:Boolean = sticky1 != sticky2;
			if(shouldSwap)
			{
				var tempPosition:Point = sticky1.CurrentPosition;
				
				sticky1.MoveStickyNoteTo(sticky2.CurrentPosition);
				sticky2.MoveStickyNoteTo(tempPosition);
			}
			else
			{
				sticky1.MoveStickyNoteTo(sticky1.CurrentPosition);
			}
			return shouldSwap;
		}
		
		public function SwapStickyNotes(sticky1:BasicSticky, sticky2:BasicSticky):void
		{
			if(swapStickyNotes(sticky1, sticky2))
			{
				submitData();
			}
		}
		
		private function throwAwayStickyNote(stickyNote:BasicSticky):void
		{
			stickyNote.MoveStickyNoteTo(new Point(m_TrashCan.x, m_TrashCan.y), false);
			clearStickyNote(stickyNote);
			
			SoundHandler.PlaySound(GSOC_SFX.SOUND_TRASHCANS[SessionInformation_GirlsTablet.Age]);
		}
		
		public function ClearStickyNote(stickyNote:BasicSticky):void
		{
			clearStickyNote(stickyNote);
			submitData();
		}
		
		public function GetStickyNoteByPosition(position:Point):BasicSticky
		{
			for(var i:int = 0; i < m_StickyNotes.length; i++)
			{
				if(m_StickyNotes[i].CurrentPosition == position)
				{
					return m_StickyNotes[i];
				}
			}
			return null;
		}
		
		public function get AvailableStickyNotes():Vector.<BasicSticky>
		{
			var availableStickyNotes:Vector.<BasicSticky> = new Vector.<BasicSticky>();

			for(var i:int = 0; i < m_StickyNotes.length; i++)
			{
				if(!m_StickyNotes[i].IsActive)
				{
					availableStickyNotes.push(m_StickyNotes[i]);
				}
			}
			return availableStickyNotes;
		}
		
		private function checkBoardStatus():void
		{
			var isComplete:Boolean = false;
			
			for(var i:int = 0; i < m_StickyNotes.length; i++)
			{
				if(m_StickyNotes[i].IsActive)
				{
					isComplete = true;
				}
			}
			
			BrainStorm_App.Dispatcher.dispatchEvent(new Event_MapStatus(Event_MapStatus.MAP_CHANGED, isComplete));
		}
		
		public function get StickyNoteLocations():Vector.<Point>
		{
			return m_StickyNotePositions;
		}
		
		public function get StickyNotes():Vector.<BasicSticky>
		{
			return m_StickyNotes;
		}
		
		public static function get Instance():StickyNoteManager
		{
			return m_Instance;
		}
	}
}
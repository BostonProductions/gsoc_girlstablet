package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.PopupManager;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Power3;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;

	public class BasicSticky extends StickyNote
	{
		private var m_CurrentPopupID:int;
		private var m_DisplayArea:BasicStickyDisplayArea;
		private var m_CurrentPosition:Point;
		private var m_IsActive:Boolean;
		
		public function BasicSticky(skin:MovieClip, displayColor:uint)
		{
			m_Skin = new DraggableSticky(skin);
			m_CurrentPosition = new Point(m_Skin.draggableObject.x, m_Skin.draggableObject.y);
			m_DisplayArea = new BasicStickyDisplayArea((m_Skin.draggableObject as MovieClip).DisplayArea, displayColor);
		}
		
		public override function Initialize():void
		{
			m_IsActive = false;
			super.Initialize();
		}
		
		public override function DeInitialize():void
		{
			super.DeInitialize();
		}
		
		public function SaveInfo(popup:BrainStormPopup):void
		{
			m_DisplayArea.DisplayInfo(popup);
		}
		
		public function ClearInfo():void
		{
			m_DisplayArea.RemoveItems();
		}
		
		protected override function StickyClicked(stickyNote:StickyNote):void
		{
			PopupManager.Instance.DisplayPopup(stickyNote as BasicSticky);
		}
		
		public function Display():void
		{
			m_Skin.draggableObject.visible = true;
			TweenLite.fromTo(m_Skin.draggableObject, .5, {scaleX: 2, scaleY: 2, alpha: 0}, {scaleX: 1, scaleY: 1, alpha: 1, ease: Back.easeOut});
			m_IsActive = true;
		}
		
		public function Hide():void
		{
			TweenLite.to(m_Skin.draggableObject, .5, {scaleX: 0, scaleY: 0, alpha: 0, onComplete: removeSkin});
			m_IsActive = false;	
			
			m_DisplayArea.RemoveItems();
		}
		
		protected override function eh_StickyNoteMoved(e:Event_DraggableObject):void
		{
			StickyNoteManager.Instance.StickyNoteMoved((this as BasicSticky), e.currentPosition);
		}
		
		private function removeSkin():void
		{
			resetPosition();
			m_Skin.draggableObject.visible = false;
		}
		
		public function MoveStickyNoteTo(position:Point, setPosition:Boolean = true):void
		{
			if(setPosition) m_CurrentPosition = position;
			TweenLite.to(m_Skin.draggableObject, .5, {x: position.x, y: position.y, ease: Power3.easeOut});
		}
		
		private function resetPosition():void
		{
			TweenLite.killTweensOf(m_Skin.draggableObject);
			
			m_Skin.draggableObject.x = m_CurrentPosition.x;
			m_Skin.draggableObject.y = m_CurrentPosition.y;
		}
		
		public function get CurrentPosition():Point
		{
			return m_CurrentPosition;
		}
		
		public function get CurrentPopupID():int
		{
			return m_CurrentPopupID;
		}
		
		public function set CurrentPopupID(value:int):void
		{
			m_CurrentPopupID = value;
		}
		
		public function get DisplayArea():BasicStickyDisplayArea
		{
			return m_DisplayArea;
		}
		
		public function get IsActive():Boolean
		{
			return m_IsActive;
		}
		
		public function get Skin():DisplayObject
		{
			return m_Skin.draggableObject;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups
{
	public class Popup_IDs
	{
		public static const SELECTION_POPUP:int = 0;
		public static const DRAWING_POPUP:int = 1;
		public static const ICONS_POPUP:int = 2;
		public static const TYPING_POPUP:int = 3;
		public static const TIPS_POPUP:int = 4;
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups
{
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes.BasicSticky;

	public class PopupManager
	{
		private static var m_Instance:PopupManager;
		
		public var m_Popups:Vector.<BrainStormPopup>;
		
		public function PopupManager()
		{
			if(m_Instance == null)
			{
				m_Instance = this;
			}
			else
			{
				throw new Error("You can only have one instance of popup manager at a time");
			}
			
			m_Popups = new Vector.<BrainStormPopup>();
		}
		
		public function Initialize():void
		{
		}
		
		public function DeInitialize():void
		{
			m_Instance = null;
			m_Popups = new Vector.<BrainStormPopup>();
		}
		
		public function AddPopup(popup:BrainStormPopup):void
		{
			m_Popups.push(popup);
		}
		
		public function DisplayPopup(stickyNote:BasicSticky):void
		{
			hidePopups();
			m_Popups[stickyNote.CurrentPopupID].Initialize();
			m_Popups[stickyNote.CurrentPopupID].CurrentStickyNote = stickyNote;
			m_Popups[stickyNote.CurrentPopupID].DisplayPopUp();
		}
		
		private function hidePopups():void
		{
			for(var i:int = 0; i < m_Popups.length; i++)
			{
				m_Popups[i].HidePopUp();
				m_Popups[i].DeInitialize();
			}
		}
		
		public static function get Instance():PopupManager
		{
			return m_Instance;
		}
	}
}
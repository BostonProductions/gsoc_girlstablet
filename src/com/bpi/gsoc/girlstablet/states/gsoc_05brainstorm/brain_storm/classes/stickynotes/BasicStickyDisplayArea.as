package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.stickynotes
{
	import com.bpi.citrusas.images.PlaceImage;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.BrainStormPopup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.drawing_popup.Drawing_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup.BrainStorm_Icon;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.brain_storm.classes.popups.popup_types.typing_popup.Typing_Popup;
	import com.greensock.TweenLite;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.text.TextField;

	public class BasicStickyDisplayArea
	{
		private var m_Bitmap:Bitmap;
		
		private var m_Icon:BrainStorm_Icon;
		private var m_Drawing:Bitmap;
		
		private var m_Skin:MovieClip;
		private var m_Container:MovieClip;
		private var m_TextField:TextField;
		
		private var m_CaptionTextfield:TextField;
		private var m_IconArea:MovieClip;
		
		private var m_Color:uint;
		
		public function BasicStickyDisplayArea(displayArea:MovieClip, color:uint)
		{
			m_Skin = displayArea;
			(m_Skin.container) ? m_Container = displayArea.container : m_Container = displayArea;
			
			m_TextField = displayArea.textbox;
			m_TextField.selectable = false;
			
			m_CaptionTextfield = displayArea.captionText;

			Utils_Text.EmbedText(m_TextField, GSOC_Info.BodyFont, 48, color);
			Utils_Text.ConvertToEmbeddedFont(m_CaptionTextfield, GSOC_Info.BodyFont, false);
			
			m_IconArea = displayArea.iconArea;
			
			m_Bitmap = new Bitmap();
			m_Color = color;
		}
		
		public function DisplayInfo(popUp:BrainStormPopup):void
		{
			RemoveItems();
			
			if(popUp is Drawing_Popup)
			{
				displayDrawing(popUp as Drawing_Popup);
			}
			else if(popUp is Typing_Popup)
			{
				displayText(popUp as Typing_Popup);
			}
			else if(popUp is Icons_Popup)
			{
				displayIcon(popUp as Icons_Popup);
			}
		}
		
		private function displayText(popup:Typing_Popup):void
		{
			m_TextField.text = popup.Text;
			Utils_Text.FitTextToTextfield(m_TextField);
		}
		
		private function displayDrawing(popup:Drawing_Popup):void
		{
			m_Drawing = Utils_Image.ConvertDisplayObjectToBitmap(popup.Drawing);

			m_Bitmap = new Bitmap(m_Drawing.bitmapData, PixelSnapping.ALWAYS, true);
			
			PlaceImage.ImageToExistingContainer(m_Bitmap, m_Skin, true);
			AgeFontLinkages.DB_BODY_FONT
		}
		
		private function displayIcon(popup:Icons_Popup):void
		{
			if(popup.Icon)
			{
				m_Icon = popup.Icon;
				
				m_CaptionTextfield.text = popup.Text;
				Utils_Text.FitTextToTextfield(m_CaptionTextfield);
				
				m_Bitmap = Utils_Image.ConvertParentToBitmap(m_Icon.Icon);
				
				TweenLite.to(m_Bitmap, 0, {colorTransform: {tint: m_Color}});
				PlaceImage.ImageToExistingContainer(m_Bitmap, m_IconArea);
			}
		}
		
		public function RemoveItems():void
		{
			m_TextField.text = "";
			if(m_IconArea.contains(m_Bitmap))
			{
				m_IconArea.removeChild(m_Bitmap);
				m_CaptionTextfield.text = "";
			}
			if(m_Skin.contains(m_Bitmap))
			{
				m_Skin.removeChild(m_Bitmap);
			}
			if(m_Container.contains(m_Bitmap))
			{
				m_Container.removeChild(m_Bitmap);
			}
			m_Icon = null;
			m_Drawing = null;
		}
		
		public function get CurrentText():String
		{
			if(m_TextField.text == "")
			{
				return Typing_Popup.DEFAULT_TEXT;
			}
			return m_TextField.text;
		}
		
		public function get CurrentIcon():BrainStorm_Icon
		{
			return m_Icon;
		}
		
		public function get CurrentDrawing():Bitmap
		{
			return m_Drawing;
		}
		
		public function get CaptionText():String
		{
			return m_CaptionTextfield.text;
		}
	}
}
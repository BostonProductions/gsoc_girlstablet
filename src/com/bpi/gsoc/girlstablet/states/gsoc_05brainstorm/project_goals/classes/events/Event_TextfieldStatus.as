package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.classes.events
{
	import flash.events.Event;

	public class Event_TextfieldStatus extends Event
	{
		public static const CHECK_TEXTFIELDS:String = "CHECK_TEXTFIELDS";
		
		public var m_IsComplete:Boolean;
		
		public function Event_TextfieldStatus(type:String, isComplete:Boolean)
		{
			super(type);
			
			m_IsComplete = isComplete;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals
{
	import com.bpi.gsoc.components.brainstorm.ProjectGoals_DB_Skin;
	import com.bpi.gsoc.framework.constants.TeamNameSizes;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class ProjectGoals_App_DB extends ProjectGoals_App
	{
		private var m_TypedSkin:ProjectGoals_DB_Skin;
		
		public function ProjectGoals_App_DB()
		{
			m_Skin = new ProjectGoals_DB_Skin();
			m_TypedSkin = (m_Skin as ProjectGoals_DB_Skin);
		}
		
		public override function Initialize():void
		{
			m_TextfieldManager.AddTextField(m_TypedSkin.answerbox_1, "How will your project help?", true);
			m_TextfieldManager.AddTextField(m_TypedSkin.answerbox_3, "Goal to solve root cause", true);
			m_TextfieldManager.AddTextField(m_TypedSkin.answerbox_2, "Science, Technology, Engineering, Math", true);
			
			m_NextButton = m_TypedSkin.next;
			
			m_IssueCause = m_TypedSkin.issuecause;
			
			super.Initialize();
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addTeamName(SessionInformation_GirlsTablet.TeamInfo.Team_Name,TeamNameSizes.DB_FONTSIZE);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			this.addChild(m_GSOCHeader);
		}
	}
}
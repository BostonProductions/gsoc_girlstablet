package com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.classes
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.events.EncapsulatedEvent;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.citrusas.text.InputTextfieldWithBackground;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.global.forestrangerCareerWindow;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.ProjectGoals_App;
	import com.bpi.gsoc.girlstablet.states.gsoc_05brainstorm.project_goals.classes.events.Event_TextfieldStatus;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;

	public class TextfieldManager
	{
		private var m_Textfields:Vector.<InputTextfieldWithBackground>;
		private var m_MaxNumChars:int;
		private var m_ForceCharLimit:Boolean;
		
		public function TextfieldManager()
		{
			m_Textfields = new Vector.<InputTextfieldWithBackground>();
		}
		
		public function Initialize():void
		{
			CitrusFramework.frameworkStage.addEventListener(KeyboardEvent.KEY_UP, eh_CheckTextfields);
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_MaxNumChars = 45;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_MaxNumChars = 55;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_MaxNumChars =60;
					break;
			}
		}
		
		public function Init():void
		{
			CitrusFramework.frameworkStage.focus = m_Textfields[0].Textfield;
			
			for(var i:int = 0; i < m_Textfields.length; i++)
			{
				EncapsulatedEvent.AddEventListener(m_Textfields[i].Textfield, Event.CHANGE, eh_RemoveHelpText, [m_Textfields[i]]);
				EncapsulatedEvent.AddEventListener(m_Textfields[i].Textfield, InputController.DOWN, eh_RemoveHelpText, [m_Textfields[i]]);
			}
		}
		
		private function eh_RemoveHelpText(inputTextfield:InputTextfieldWithBackground):void
		{
			EncapsulatedEvent.RemoveEventListener(inputTextfield.Textfield, Event.CHANGE);
			EncapsulatedEvent.RemoveEventListener(inputTextfield.Textfield, InputController.DOWN);
			
			inputTextfield.ClearHelpText();
		}
		
		public function DeInitialize():void
		{
			for(var i:int = 0; i < m_Textfields.length; i++)
			{
				m_Textfields[i].DeInitializeEventListeners();
			}
			m_Textfields = null;
			CitrusFramework.frameworkStage.removeEventListener(KeyboardEvent.KEY_UP, eh_CheckTextfields);
		}
		
		public function AddTextField(textfield:MovieClip, helpText:String, forceCharLimit:Boolean = false):void
		{
			var inputTextfield:InputTextfieldWithBackground = new InputTextfieldWithBackground(textfield.textfield, textfield, GSOC_Info.ColorOne, false, helpText);
			
			textfield.gotoAndStop(1);
			
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR) Utils_Text.ConvertToEmbeddedFont(textfield.textfield, AgeFontLinkages.SA_BODYBOLD_FONT);
			else Utils_Text.ConvertToEmbeddedFont(textfield.textfield, GSOC_Info.BodyFont);
			inputTextfield.InitializeEventListeners();
			inputTextfield.Textfield.multiline = false;
			inputTextfield.Textfield.maxChars = m_MaxNumChars;

			m_Textfields.push(inputTextfield);
			
			m_ForceCharLimit = forceCharLimit;
		}
		
	
		
		private function eh_CheckTextfields(e:KeyboardEvent):void
		{
			var isComplete:Boolean = true;			
			for(var i:int = 0; i < m_Textfields.length; i++)
			{
				if(!m_Textfields[i].IsPopulated)
				{
					isComplete = false;
				}
				if(m_ForceCharLimit)
				{
					if(m_Textfields[i].Text.length > m_MaxNumChars)
					{
						m_Textfields[i].Textfield.text = m_Textfields[i].Text.substr(0, m_MaxNumChars);
					}
				}
			}
			
			ProjectGoals_App.Dispatcher.dispatchEvent(new Event_TextfieldStatus(Event_TextfieldStatus.CHECK_TEXTFIELDS, isComplete));
		}
	}
}
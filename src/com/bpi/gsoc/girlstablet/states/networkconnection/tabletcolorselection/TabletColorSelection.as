package com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection
{
	import com.bpi.gsoc.components.girlstablet.general.TabletColor_00;
	import com.bpi.gsoc.components.girlstablet.general.TabletColor_01;
	import com.bpi.gsoc.components.girlstablet.general.TabletColor_02;
	import com.bpi.gsoc.components.girlstablet.general.TabletColor_03;
	import com.bpi.gsoc.girlstablet.states.networkconnection.AppState_NetworkConnection;
	import com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection.events.Event_TabletColor;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class TabletColorSelection extends Sprite
	{
		private var m_TabletColorList:Vector.<TabletColor>;
		private var m_TabletColorList_Position:Point = new Point(640, 512);
		private var m_TabletColorList_Offset:Point = new Point(384, 0);
		
		public function TabletColorSelection()
		{
			m_TabletColorList = new Vector.<TabletColor>();
		}
		
		public function InitializeEventListeners():void
		{
			AppState_NetworkConnection.NetworkConnectionEventDispatcher.addEventListener(Event_TabletColor.EVENT_SET_ACTIVE_TABLETCOLOR, eh_SetActiveTabletColor);
		}
		
		public function Initialize():void
		{
			initializeTabletColorList();
		}
		
		public function Update():void
		{
			updateTabletColorList();
		}
		
		public function Init():void
		{
			
		}
		
		public function DeInit():void
		{
			
		}
		
		private function eh_SetActiveTabletColor(evt:Event_TabletColor):void
		{
			setActiveTabletColor(evt.E_Index);
		}
		
		private function initializeTabletColorList():void
		{
			m_TabletColorList.push(new TabletColor(TabletColor_00));
			m_TabletColorList.push(new TabletColor(TabletColor_01));
			m_TabletColorList.push(new TabletColor(TabletColor_02));
			m_TabletColorList.push(new TabletColor(TabletColor_03));
			
			var position:Point = m_TabletColorList_Position.clone();
			for(var index:int = 0; index < m_TabletColorList.length; index++)
			{
				m_TabletColorList[index].addEventListener(Event_TabletColor.EVENT_SET_ACTIVE_TABLETCOLOR, eh_SetActiveTabletColor);
				m_TabletColorList[index].Initialize(index);
				m_TabletColorList[index].SetPosition(position);
				this.addChild(m_TabletColorList[index]);
				
				position.x += m_TabletColorList_Offset.x;
				position.y += m_TabletColorList_Offset.y;
			}
		}
		
		private function updateTabletColorList():void
		{
			for each(var tabletColor:TabletColor in m_TabletColorList)
			{
				tabletColor.Update();
			}
		}
		
		private function setActiveTabletColor(index:int):void
		{
			for each(var tabletColor:TabletColor in m_TabletColorList)
			{
				tabletColor.ToggleActive(tabletColor.Index == index);
			}
			
			AppState_NetworkConnection.NetworkConnectionEventDispatcher.dispatchEvent(new Event_TabletColor(Event_TabletColor.EVENT_SAVE_ACTIVE_TABLETCOLOR, index));
		}
	}
}
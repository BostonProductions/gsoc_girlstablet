package com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.gsoc.girlstablet.states.networkconnection.tabletcolorselection.events.Event_TabletColor;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class TabletColor extends Sprite
	{
		private var m_AnimationClip:AnimationClip;
		private var m_AnimationClip_ImageClass:Class;
		
		private var m_Index:int;
		
		public function TabletColor(imageClass:Class)
		{
			m_AnimationClip_ImageClass = imageClass;
			m_Index = -1;
		}
		
		public function Initialize(index:int):void
		{
			m_AnimationClip = new AnimationClip(m_AnimationClip_ImageClass);
			
			m_Index = index;
			
			this.addChild(m_AnimationClip);
		}
		
		public function Update():void
		{
			checkSelected();
		}
		
		private function checkSelected():void
		{
			if(Input.IsMouseClicked && m_AnimationClip.GetBounds(CitrusFramework.frameworkStage).containsPoint(Input.MousePosition))
			{
				this.dispatchEvent(new Event_TabletColor(Event_TabletColor.EVENT_SET_ACTIVE_TABLETCOLOR, m_Index));
			}
		}
		
		public function SetPosition(position:Point):void
		{
			m_AnimationClip.SetPosition(position);
		}
		
		public function ToggleActive(toggle:Boolean):void
		{
			m_AnimationClip.Frame = toggle ? 1 : 0;
		}
		
		public function get Index():int
		{
			return m_Index;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster
{
	import com.bpi.components.poster.Poster_SA_Skin;
	import com.bpi.gsoc.components.brainstorm.IconBox_SA_Skin;
	import com.bpi.gsoc.components.global.balanceSkill;
	import com.bpi.gsoc.components.global.booksSkill;
	import com.bpi.gsoc.components.global.broomSkill;
	import com.bpi.gsoc.components.global.computerSkill;
	import com.bpi.gsoc.components.global.drawingtoolSkill;
	import com.bpi.gsoc.components.global.earSkill;
	import com.bpi.gsoc.components.global.handshakeSkill;
	import com.bpi.gsoc.components.global.heartpulseSkill;
	import com.bpi.gsoc.components.global.lightbulbSkill;
	import com.bpi.gsoc.components.global.magnifyingGlassSkill;
	import com.bpi.gsoc.components.global.microphoneSkill;
	import com.bpi.gsoc.components.global.quotesSkill;
	import com.bpi.gsoc.framework.gsocui.ColorPicker;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.icons_popup.Icons_Popup;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.typing_popup.Typing_Popup;
	
	import flash.display.Bitmap;

	public class Poster_App_SA extends Poster_App
	{
		private var m_TypedSkin:Poster_SA_Skin;
		
		public function Poster_App_SA()
		{
			m_Skin = new Poster_SA_Skin();
			m_TypedSkin = (m_Skin as Poster_SA_Skin);
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
		}
		
		public override function Initialize():void
		{
			m_Poster = m_TypedSkin.poster;
			m_SubmitButton = m_TypedSkin.submit;
			m_Border = m_TypedSkin.poster.borders;
			m_Pattern = m_TypedSkin.poster.background;
			m_Background = m_TypedSkin.poster.maxArea;
			m_ColorSelection = new ColorPicker(m_TypedSkin.gradient, m_TypedSkin, "COLOR_CHANGED");
			
			m_Border_Buttons.push(m_TypedSkin.outline0);
			m_Border_Buttons.push(m_TypedSkin.outline1);
			m_Border_Buttons.push(m_TypedSkin.outline2);
			m_Border_Buttons.push(m_TypedSkin.outline3);
			m_Border_Buttons.push(m_TypedSkin.outline4);
			
			m_Pattern_Buttons.push(m_TypedSkin.pattern0);
			m_Pattern_Buttons.push(m_TypedSkin.pattern1);
			m_Pattern_Buttons.push(m_TypedSkin.pattern2);
			m_Pattern_Buttons.push(m_TypedSkin.pattern3);
			m_Pattern_Buttons.push(m_TypedSkin.pattern4);
			
			m_TrashCan = m_TypedSkin.garbagebin;
			
			m_Popup_Buttons.push(m_TypedSkin.icons);
			m_Popup_Buttons.push(m_TypedSkin.type);
			
			var iconsPopup:Icons_Popup = new Icons_Popup(m_TypedSkin.icons_Popup);
			iconsPopup.Background = IconBox_SA_Skin;
			iconsPopup.AddSkillIcon(new Bitmap(new balanceSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new booksSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new broomSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new computerSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new drawingtoolSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new earSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new handshakeSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new heartpulseSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new lightbulbSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new magnifyingGlassSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new microphoneSkill()));
			iconsPopup.AddSkillIcon(new Bitmap(new quotesSkill()));
			m_Popups.push(iconsPopup);
			m_Popups.push(new Typing_Popup(m_TypedSkin.typing_Popup));
			
			m_CanResize = true;
			m_CanRotate = true;
			
			super.Initialize();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.icons_popup
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.ui.swipeablethings.SwipeableContent;
	import com.bpi.citrusas.ui.swipeablethings.SwipeableContent_NoVelocity;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_11poster.classes.popups.PosterPopup;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;

	public class Icons_Popup extends PosterPopup
	{
		private static const CLICK_REGISTRATION_TIME:Number = 200;
		private static const MIN_DRAG_LENGTH:Number = 50;
		
		private var m_SkillIcons:Vector.<Poster_Icon>;
		private var m_SwipeableContent:SwipeableContent_NoVelocity;
		private var m_CurrentIcon:Poster_Icon;
		
		private var m_ClickTimer:Number = 0;
		
		private var m_BackgroundClass:Class;
		
		private var m_MouseIconClickStartPosition:Point;
		
		public function Icons_Popup(skin:MovieClip)
		{
			m_SkillIcons = new Vector.<Poster_Icon>();
			m_SwipeableContent = new SwipeableContent_NoVelocity(skin.contentArea, .5, 3, 1);
			super(skin);
		}
		
		public function AddSkillIcon(icon:Bitmap):void
		{
			icon.smoothing = true;
			var brainStormIcon:Poster_Icon = new Poster_Icon(icon, m_BackgroundClass);
			m_SwipeableContent.AddItem(brainStormIcon, new Point(50,30));
			m_SkillIcons.push(brainStormIcon);
		}
		
		public override function DisplayPopUp():void
		{
			super.DisplayPopUp();
			
			deselectIcon();
			
			m_Skin.save.visible = false;
			m_Skin.clear.visible = false;
			
			m_Skin.RightArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			m_Skin.LeftArrow.addEventListener(InputController.DOWN, eh_ArrowPressed);
			
			m_Skin.contentArea.addEventListener(InputController.DOWN, eh_Icon_Clicked);
			m_Skin.contentArea.addEventListener(InputController.UP, eh_Icon_Clicked);
			
			m_SwipeableContent.Initialize();
		}
		
		public override function HidePopUp():void
		{
			super.HidePopUp();
			
			m_Skin.RightArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);
			m_Skin.LeftArrow.removeEventListener(InputController.DOWN, eh_ArrowPressed);

			m_Skin.contentArea.removeEventListener(InputController.DOWN, eh_Icon_Clicked);
			m_Skin.contentArea.removeEventListener(InputController.UP, eh_Icon_Clicked);
			
			m_SwipeableContent.DeInitialize();
		}
		
		private function eh_ArrowPressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if(e.target.name == "RightArrow")
			{
				m_SwipeableContent.MoveTo(3);
			}
			else
			{
				m_SwipeableContent.MoveTo(-3);
			}
		}
		
		public function eh_Icon_Clicked(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CHOOSEICONS[SessionInformation_GirlsTablet.Age]);
			if(e.type == MouseEvent.MOUSE_DOWN || e.type == TouchEvent.TOUCH_BEGIN)
			{
				m_ClickTimer = getTimer();
				if(e is MouseEvent)
				{
					m_MouseIconClickStartPosition = new Point((e as MouseEvent).stageX, (e as MouseEvent).stageY);
				}
				else if(e is TouchEvent)
				{
					m_MouseIconClickStartPosition = new Point((e as TouchEvent).stageX, (e as TouchEvent).stageY);
				}
			}
			else
			{
				var endMousePoint:Point = new Point();
				if(e is MouseEvent)
				{
					endMousePoint = new Point((e as MouseEvent).stageX, (e as MouseEvent).stageY);
				}
				else if(e is TouchEvent)
				{
					endMousePoint = new Point((e as TouchEvent).stageX, (e as TouchEvent).stageY);
				}
				
				if((getTimer() - m_ClickTimer) < CLICK_REGISTRATION_TIME && e.target is Poster_Icon)
				{
					deselectIcon();
					m_CurrentIcon = (e.target as Poster_Icon);
					selectIcon();
				}
				else if(e.target is Poster_Icon && Point.distance(m_MouseIconClickStartPosition, endMousePoint) < MIN_DRAG_LENGTH)
				{
					deselectIcon();
					m_CurrentIcon = (e.target as Poster_Icon);
					selectIcon();
				}
			}
		}	
		
		private function deselectIcon():void
		{
			if(m_CurrentIcon)
			{
				m_CurrentIcon.DeSelect();
				m_CurrentIcon = null;
				m_Skin.save.visible = false;
				m_Skin.clear.visible = false;
			}
		}
		
		private function selectIcon():void
		{
			m_Skin.save.visible = true;
			m_Skin.clear.visible = true;
			m_CurrentIcon.Select();
		}
		
		protected override function eh_ClearButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			deselectIcon();
			super.eh_ClearButton_Pressed(e);
		}
		
		protected override function eh_SaveButton_Pressed(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			super.eh_SaveButton_Pressed(e);
		}
		
		public function set Background(background:Class):void
		{
			m_BackgroundClass = background;
		}
		
		public function get Icon():Poster_Icon
		{
			if(m_CurrentIcon)
			{
				return m_CurrentIcon;
			}
			return null;
		}
	}
}
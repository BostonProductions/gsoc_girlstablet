package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice
{
	import com.bpi.gsoc.components.chooseyourcareer.CareerChoice_Screen1_JC_Skin;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class CareerChoice_App_JC extends CareerChoice_App
	{
		public function CareerChoice_App_JC()
		{
			super();
			
			m_Skin = new CareerChoice_Screen1_JC_Skin();
			m_Glow = AgeColors.JC_GLOW;
			m_HeaderFont = AgeFontLinkages.JC_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.JC_BODY_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addPagination();
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}
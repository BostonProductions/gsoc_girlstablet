package com.bpi.gsoc.girlstablet.states.gsoc_03chooseyourcareer.career_choice.classes
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.career.Career;
	import com.bpi.gsoc.framework.career.group.CareerGroupReference;
	import com.bpi.gsoc.girlstablet.chooseyourcareer.CareerMask;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class CareerChoiceBlock extends Sprite
	{
		private var _asset:MovieClip
		private var _linkedCareer:Career;
		private var _headerText:TextField;
		private var _bodyText:TextField;
		private var _avatarMarker:MovieClip;
		private var _background:MovieClip;
		private var _dividerBar:MovieClip;
		
		private static const _CAREER_CONTENTS_SPACER:Number = 25;
		
		public static const CAREER_BLOCK_SELECTED:String = "CAREER_BLOCK_SELECTED";
		
		public function CareerChoiceBlock(mc:MovieClip, headerFont:String, bodyFont:String)
		{
			super();
			this._asset = mc;
			this._headerText = this._asset.career_Description_Label.careerTitle.job_Description_Center;
			this._bodyText = this._asset.career_Description_Label.career_Text_Description_Label.job_Description_Center;
			this._avatarMarker = this._asset.avatarMarker;
			this._dividerBar = this._asset.career_Description_Label.dividerBar;
			
			this.setUpBasicTextField(this._headerText, headerFont);
			this.setUpBasicTextField(this._bodyText, bodyFont);
			
			this._asset.career_Description_Label.y = this._asset.backgroundFrame.y + this._asset.backgroundFrame.height;
			
		}
		
		private function setUpBasicTextField(tb:TextField, font:String):void
		{
			var oldDefault:TextFormat = tb.defaultTextFormat;
			var tf:TextFormat = new TextFormat(font, oldDefault.size, oldDefault.color, oldDefault.bold,
				oldDefault.italic, oldDefault.underline, oldDefault.url, oldDefault.target, oldDefault.align, 
				oldDefault.leftMargin, oldDefault.rightMargin, oldDefault.indent, oldDefault.leading);
			
			tb.defaultTextFormat = tf;
			tb.embedFonts = true;
			tb.setTextFormat(tf);
			tb.wordWrap = true;
			tb.autoSize = TextFieldAutoSize.CENTER;
		}
		
		public function Init(callback:Function):void
		{
			this._asset.gotoAndStop(1);
			
			this._asset.mouseEnabled = true;
			this._asset.mouseChildren = false;
			this._asset.addEventListener(InputController.CLICK, callback);
			this._asset.addEventListener(InputController.CLICK, this.BlockSelected);
		}
		
		public function DeInit(callback:Function):void
		{
			this._asset.gotoAndStop(0);
			this._asset.removeEventListener(InputController.CLICK, callback);
			this._asset.removeEventListener(InputController.CLICK, this.BlockSelected);
		}
		
		public function BlockSelected(e:Event):void
		{
			this.dispatchEvent(new Event(CareerChoiceBlock.CAREER_BLOCK_SELECTED));
		}
		
		public function SetCareerText(c:Career):void
		{
			this.setHeaderText(c.title.toLocaleUpperCase());
			this.setBodyText(c.description);
			this.distributeCareerBoxTextElements();
		}
		
		private function setHeaderText(s:String):void
		{
			this._headerText.text = s;
			this._headerText.height = this._headerText.textHeight;
			this.distributeCareerBoxTextElements();
			
		}
		
		private function distributeCareerBoxTextElements():void
		{
			this._dividerBar.y = this._headerText.y + this._headerText.height + _CAREER_CONTENTS_SPACER * 0.5; // title has weird spacing around it
			this._bodyText.parent.y = this._dividerBar.y + _CAREER_CONTENTS_SPACER;
			
		}
		
		public function setBodyText(s:String):void
		{
			this._bodyText.text = s;
		}
		
		public function get linkedCareer():Career
		{
			return _linkedCareer;
		}

		public function set linkedCareer(value:Career):void
		{
			_linkedCareer = value;
			this.SetCareerText(value);
			this.setBackground(CareerGroupReference.GetBackgroundForCareer(value.title));
		}
		
		

		public function set avatarMarker(value:GSOC_Avatar_Skin):void
		{
			
			this._asset.backgroundFrame.addChildAt(value, 1);
			
			
			value.mouseEnabled = false;
			value.mouseChildren = false;
			var ovalMask:CareerMask = new CareerMask();

			this._asset.backgroundFrame.addChild(ovalMask);
			
			value.scaleX = value.scaleY = 0.5;
			value.x = this._asset.backgroundFrame.width / 2 - value.width / 2 - 20; // offset because of drop shadow
			value.y = this._asset.backgroundFrame.height / 10;
			value.mask = ovalMask;
		}

		public function get asset():MovieClip
		{
			return _asset;
		}

		public function set asset(value:MovieClip):void
		{
			_asset = value;
		}
		
		public function setBackground(mc:MovieClip):void
		{
			this._background = mc;
			if((this._asset.backgroundFrame as MovieClip).numChildren > 0) (this._asset.backgroundFrame as MovieClip).removeChildAt(0);
			
			var ovalMask:CareerMask = new CareerMask();
			
			_background.addChild(ovalMask);
			this._asset.backgroundFrame.addChildAt(_background, 0);
			ovalMask.alpha = 0.5;
			_background.mask = ovalMask;
		}
	}
}
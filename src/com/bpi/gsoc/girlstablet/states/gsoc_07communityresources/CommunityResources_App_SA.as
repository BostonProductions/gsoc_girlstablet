package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.web.WebView;
	import com.bpi.gsoc.components.communityrescources.ResourceSelection_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.framework.gsocui.GSOC_WebView;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class CommunityResources_App_SA extends CommunityResources_App
	{
		public var m_BodyBoldFont:String;
		
		private var m_WebView:WebView;
		private var m_LaunchWebButton:MovieClip;
		
		public function CommunityResources_App_SA()
		{
			m_WebView = new GSOC_WebView();
			m_Skin = new ResourceSelection_SA_Skin();
			m_HeaderFont = AgeFontLinkages.SA_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.SA_BODY_FONT;
			m_BodyBoldFont = AgeFontLinkages.SA_BODYBOLD_FONT;
			m_LaunchWebButton = m_Skin.LaunchWebButton;
			m_GSOCHeader = new GSOC_Header(m_Skin.Header.paginationArea);
			GSOCheader.addArrows(m_Skin.Header.paginationArea.x, 0);
			super.Initialize();
			
			m_LaunchWebButton.addEventListener(InputController.DOWN, eh_LaunchWeb);
		}
		
		private function eh_LaunchWeb(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_LAUNCHWEB);
			m_WebView.Initialize();
		}
		
		public override function DeInitialize():void
		{
			m_WebView.DeInitialize();
			super.DeInitialize();
		}
	}
}


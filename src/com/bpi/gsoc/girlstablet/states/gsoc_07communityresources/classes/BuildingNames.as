package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes
{
	public class BuildingNames
	{
		public static const DB_NEWSPAPER:String = "Newspaper";
		public static const DB_HOME:String = "Home";
		public static const DB_COMMUNITYCENTER:String = "Community center";
		public static const DB_CLASSROOM:String = "Classroom";
		public static const DB_TOWNHALL:String = "Town hall";
		public static const DB_LIBRARY:String = "Library";
		public static const DB_GSMEETING:String = "Girl scout meeting";
		
		public static const JC_PUBLICLIBRARY:String = "Public library";
		public static const JC_RECDEPT:String = "Recreation Department";
		public static const JC_CLASSROOM:String = "Classroom";
		public static const JC_PTA:String = "PTA";
		public static const JC_NEWS:String = "News station";
		public static const JC_HOME:String = "Home";
		public static const JC_FOODBANK:String = "Food bank";
		public static const JC_GSLEADER:String = "Girl Scout leader";
		public static const JC_CITYHALL:String = "City hall";
		public static const JC_GROCERYSTORE:String = "Grocery store";
		
		public static const SA_COUNTYOFFICE:String = "County office building";
		public static const SA_ACADEMICLIBRARY:String = "Academic library";
		public static const SA_PUBACCESSTV:String = "Public access TV";
		public static const SA_GSCOMMUNITY:String = "Girl Scout community";
		public static const SA_CIVICCENTER:String = "Civic center";
		public static const SA_BOARDOFED:String = "Board of education";
		public static const SA_BANK:String = "Bank";
		
		public static const ADD_YOUR_OWN_1:String = "ADD YOUR OWN 1";
		public static const ADD_YOUR_OWN_2:String = "ADD YOUR OWN 2";
		public static const ADD_YOUR_OWN_3:String = "ADD YOUR OWN 3";
		public static const ADD_YOUR_OWN_4:String = "ADD YOUR OWN 4";
		public static const ADD_YOUR_OWN_5:String = "ADD YOUR OWN 5";
	}
}
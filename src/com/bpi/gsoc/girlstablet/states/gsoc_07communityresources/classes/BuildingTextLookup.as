package com.bpi.gsoc.girlstablet.states.gsoc_07communityresources.classes
{
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;

	public class BuildingTextLookup
	{
		private static var m_InstanceNames:Vector.<String> = new Vector.<String>();
		private static var m_HumanReadableNames:Vector.<String> = new Vector.<String>();
		
		private static function Initialize():void
		{
			Clear();
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_InstanceNames.push("building_newspaper")
					m_HumanReadableNames.push(BuildingNames.DB_NEWSPAPER);
				
					m_InstanceNames.push("building_home");
					m_HumanReadableNames.push(BuildingNames.DB_HOME);
					
					m_InstanceNames.push("building_communitycenter");
					m_HumanReadableNames.push(BuildingNames.DB_COMMUNITYCENTER);
					
					m_InstanceNames.push("building_classroom");
					m_HumanReadableNames.push(BuildingNames.DB_CLASSROOM);
					
					m_InstanceNames.push("building_townhall");
					m_HumanReadableNames.push(BuildingNames.DB_TOWNHALL);
					
					m_InstanceNames.push("building_library");
					m_HumanReadableNames.push(BuildingNames.DB_LIBRARY);
					
					m_InstanceNames.push("building_girlscoutmeeting");
					m_HumanReadableNames.push(BuildingNames.DB_GSMEETING);
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_InstanceNames.push("building_publiclibrary");
					m_HumanReadableNames.push(BuildingNames.JC_PUBLICLIBRARY);
					
					m_InstanceNames.push("building_recdept");
					m_HumanReadableNames.push(BuildingNames.JC_RECDEPT);
					
					m_InstanceNames.push("building_classroom");
					m_HumanReadableNames.push(BuildingNames.JC_CLASSROOM);
					
					m_InstanceNames.push("building_pta");
					m_HumanReadableNames.push(BuildingNames.JC_PTA);
					
					m_InstanceNames.push("building_newsstation");
					m_HumanReadableNames.push(BuildingNames.JC_NEWS);
					
					m_InstanceNames.push("building_home");
					m_HumanReadableNames.push(BuildingNames.JC_HOME);
					
					m_InstanceNames.push("building_foodbank");
					m_HumanReadableNames.push(BuildingNames.JC_FOODBANK);
					
					m_InstanceNames.push("building_girlscoutleader");
					m_HumanReadableNames.push(BuildingNames.JC_GSLEADER);
					
					m_InstanceNames.push("building_cityhall");
					m_HumanReadableNames.push(BuildingNames.JC_CITYHALL);
					
					m_InstanceNames.push("building_grocerystore");
					m_HumanReadableNames.push(BuildingNames.JC_GROCERYSTORE);
					
					m_InstanceNames.push("building_addyourown1");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_1);
					
					m_InstanceNames.push("building_addyourown2");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_2);
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_InstanceNames.push("building_countyofficebuilding");
					m_HumanReadableNames.push(BuildingNames.SA_COUNTYOFFICE);
					
					m_InstanceNames.push("building_academiclibrary");
					m_HumanReadableNames.push(BuildingNames.SA_ACADEMICLIBRARY);
					
					m_InstanceNames.push("building_publicaccesstv");
					m_HumanReadableNames.push(BuildingNames.SA_PUBACCESSTV);
					
					m_InstanceNames.push("building_girlscoutcommunity");
					m_HumanReadableNames.push(BuildingNames.SA_GSCOMMUNITY);
					
					m_InstanceNames.push("building_civiccenter");
					m_HumanReadableNames.push(BuildingNames.SA_CIVICCENTER);
					
					m_InstanceNames.push("building_bank");
					m_HumanReadableNames.push(BuildingNames.SA_BANK);
					
					m_InstanceNames.push("building_boardofeducation");
					m_HumanReadableNames.push(BuildingNames.SA_BOARDOFED);
					
					m_InstanceNames.push("building_addyourown1");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_1);
					
					m_InstanceNames.push("building_addyourown2");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_2);
					
					m_InstanceNames.push("building_addyourown3");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_3);
					
					m_InstanceNames.push("building_addyourown4");
					m_HumanReadableNames.push(BuildingNames.ADD_YOUR_OWN_4);
					break;
			}
		}
		
		private static function idiotCheckLists():void
		{
			if(m_InstanceNames.length != m_HumanReadableNames.length)
			{
				throw(new Error("The number of building names vs the number of building instance names is off. The length" +
					" of the building names is " + m_HumanReadableNames.length + " and the length of the instance names is " + m_InstanceNames + "."));
			}
		}
		
		public static function GetHumanReadableNameFromInstanceName(instanceName:String):String
		{
			if(m_InstanceNames.length == 0) Initialize();
			idiotCheckLists();
			
			if(m_InstanceNames.indexOf(instanceName) != -1)
			{
				return m_HumanReadableNames[m_InstanceNames.indexOf(instanceName)];
			}
			else
			{
				return "";
			}	
		}
		
		public static function GetInstanceNameFromHumanReadableName(humanReadableName:String):String
		{
			if(m_InstanceNames.length == 0) Initialize();
			idiotCheckLists();
			
			if(m_HumanReadableNames.indexOf(humanReadableName) != -1)
			{
				return m_InstanceNames[m_HumanReadableNames.indexOf(humanReadableName)];
			}
			else
			{
				return "";
			}	
		}
	
		public static function Clear():void
		{
			m_HumanReadableNames = new Vector.<String>();
			m_InstanceNames = new Vector.<String>();
		}
	}
}
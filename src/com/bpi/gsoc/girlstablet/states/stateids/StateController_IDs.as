package com.bpi.gsoc.girlstablet.states.stateids
{
	public class StateController_IDs
	{
		public static const STATECONTROLLER_MAIN:int = 0;
		public static const STATECONTROLLER_ASSEMBLEYOURTEAM:int = 1;
		public static const STATECONTROLLER_EXPLOREYOURPASSION:int = 2;
		public static const STATECONTROLLER_CHOOSEYOURCAREER:int = 3;
		public static const STATECONTROLLER_KNOWYOURROLE:int = 4;
		public static const STATECONTROLLER_COMMUNITYRESOURCES:int = 5; 
		public static const STATECONTROLLER_BRAINSTORM:int = 6;
		public static const STATECONTROLLER_PROJECTPLANNING:int = 7;
		public static const STATECONTROLLER_CHOOSEISSUE:int = 8;
		public static const STATECONTROLLER_MINDMAP:int = 9;
		public static const STATECONTROLLER_PICTUREYOURSELF:int = 10;
		public static const STATECONTROLLER_RECORDVIDEO:int = 11;
		public static const STATECONTROLLER_POSTER:int = 12;
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.multiple_questions
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.citrusas.ui.draggableobject.events.Event_DraggableObject;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.career.algorithm.AnswerDataModel;
	import com.bpi.gsoc.framework.career.algorithm.CareerDataModel;
	import com.bpi.gsoc.framework.career.algorithm.CareerDataUtils;
	import com.bpi.gsoc.framework.career.algorithm.QuestionDataModel;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_ExploreYourPassion;
	import com.greensock.TweenMax;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class MultipleQuestions_State extends GSOC_AppState
	{
		private var m_AppState:MultipleQuestions_App;
		public var _questions:Array;
		public var _questions1:Array;
		public var _questions2:Array;
		public var _questions3:Array;
		private var _whichQuestion:int;
		private var _whichText:int;
		private var _curSelection:int;
		private var _answerArray:Array;
		private var _questionOrder:Array;
		public var _ageRange:int = 0;
		private var saveLocY:int;
		private var questionsXML:XML;
		private var sliderDraggableObject:DraggableObject;
		
		private var m_MultipleQuestion_Group:Vector.<MultipleQuestions_App>;
		
		public function MultipleQuestions_State()
		{
			super(StateController_StateIDs_ExploreYourPassion.STATE_MULTIPLEQUESTIONS);
		}
		
		public override function Initialize():void
		{	
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case 0:
					m_AppState = new MultipleQuestions_App_DB();
					break;
				case 1:
					m_AppState = new MultipleQuestions_App_JC();
					break;
				case 2:
					m_AppState = new MultipleQuestions_App_SA();
					break;
				default:
					m_AppState = new MultipleQuestions_App_DB();
			}
			
			m_AppState.Init();
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			m_AppState.skin.save_Answer_Button.y = CitrusFramework.frameworkStageSize.y - m_AppState.skin.save_Answer_Button.height;
			saveLocY = m_AppState.skin.save_Answer_Button.y;
			m_AppState.skin.save_Answer_Button.x = ButtonPlacement.NEXT_X;
				
			CareerDataUtils.CareerDataLoader.addEventListener(CareerDataUtils.CAREER_DATA_LOADED, this.eh_CareerDataLoaded);
			CareerDataUtils.LoadCareerMatrix(SessionInformation_GirlsTablet.Age, SessionInformation_GirlsTablet.Journey);
		}
		
		private function eh_CareerDataLoaded(evt:Event):void
		{
			parseQuestions(1);
			parseQuestions(2);
			parseQuestions(3);
			
			this._whichQuestion = 0;
			this._whichText = 0;
			this._curSelection = 0;
			this._answerArray = new Array(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
			this._questionOrder = new Array(0, 1, 2, 3, 4, 5);
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					this._questions = this._questions1;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					this._questions = this._questions2;
					m_AppState.skin.answerMechanism.answerField1.text = this._questions[0][1].toUpperCase();
					m_AppState.skin.answerMechanism.answerField2.text = this._questions[0][2].toUpperCase();
					m_AppState.skin.answerMechanism.answerField3.text = this._questions[0][3].toUpperCase();
					m_AppState.skin.answerMechanism.answerField4.text = this._questions[0][4].toUpperCase();
					m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(0);
					m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
					m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(0);
					m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(0);
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					this._questions = this._questions3;
					m_AppState.skin.answerMechanism.answerField1.text = this._questions[0][1].toUpperCase();
					m_AppState.skin.answerMechanism.answerField2.text = this._questions[0][2].toUpperCase();
					m_AppState.skin.answerMechanism.answerField3.text = this._questions[0][3].toUpperCase();
					m_AppState.skin.answerMechanism.answerField4.text = this._questions[0][4].toUpperCase();
					m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(0);
					m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
					m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(0);
					m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(0);
					break;
			}
			
			var tf:TextFormat;
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				tf = new TextFormat(m_AppState.headerFont, (m_AppState.skin.questionWrapper.question_TextField as TextField).defaultTextFormat.size, (m_AppState.skin.questionWrapper.question_TextField as TextField).defaultTextFormat.color, false);
			}
			else
			{
				tf = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.questionWrapper.question_TextField as TextField).defaultTextFormat.size, (m_AppState.skin.questionWrapper.question_TextField as TextField).defaultTextFormat.color, false);
			}
			tf.align = TextFormatAlign.CENTER;

			var questionTextfield:TextField = (m_AppState.skin.questionWrapper.question_TextField as TextField);
			questionTextfield.defaultTextFormat = tf;
			questionTextfield.embedFonts = true;
			questionTextfield.setTextFormat(tf);
			questionTextfield.text = this._questions[0][0];
			Utils_Text.FitTextToTextfield(questionTextfield, 4);
			
			
			if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
			{
				var tf3:TextFormat = new TextFormat(m_AppState.headerFont, (m_AppState.skin.answerMechanism.answerField1 as TextField).defaultTextFormat.size, (m_AppState.skin.answerMechanism.answerField1 as TextField).defaultTextFormat.color, false);
				tf3.align = TextFormatAlign.CENTER;
				
				(m_AppState.skin.answerMechanism.answerField1 as TextField).defaultTextFormat = tf3;
				(m_AppState.skin.answerMechanism.answerField1 as TextField).embedFonts = true;
				(m_AppState.skin.answerMechanism.answerField1 as TextField).setTextFormat(tf3);
				
				(m_AppState.skin.answerMechanism.answerField2 as TextField).defaultTextFormat = tf3;
				(m_AppState.skin.answerMechanism.answerField2 as TextField).embedFonts = true;
				(m_AppState.skin.answerMechanism.answerField2 as TextField).setTextFormat(tf3);
				
				(m_AppState.skin.answerMechanism.answerField3 as TextField).defaultTextFormat = tf3;
				(m_AppState.skin.answerMechanism.answerField3 as TextField).embedFonts = true;
				(m_AppState.skin.answerMechanism.answerField3 as TextField).setTextFormat(tf3);
				
				(m_AppState.skin.answerMechanism.answerField4 as TextField).defaultTextFormat = tf3;
				(m_AppState.skin.answerMechanism.answerField4 as TextField).embedFonts = true;
				(m_AppState.skin.answerMechanism.answerField4 as TextField).setTextFormat(tf3);
				m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[0]][1].toUpperCase();
				m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[0]][2].toUpperCase();
				m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[0]][3].toUpperCase();
				m_AppState.skin.answerMechanism.answerField4.text = this._questions[this._questionOrder[0]][4].toUpperCase();
			}
			
			var tf2:TextFormat;
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				tf2 = new TextFormat(m_AppState.headerFont, (m_AppState.skin.questionNumberWrapper.question_Number as TextField).defaultTextFormat.size, (m_AppState.skin.questionNumberWrapper.question_Number as TextField).defaultTextFormat.color, false);
			}
			else
			{
				tf2 = new TextFormat(m_AppState.bodyFont, (m_AppState.skin.questionNumberWrapper.question_Number as TextField).defaultTextFormat.size, (m_AppState.skin.questionNumberWrapper.question_Number as TextField).defaultTextFormat.color, false);
			}
			tf2.align = TextFormatAlign.CENTER;
			
			(m_AppState.skin.questionNumberWrapper.question_Number as TextField).defaultTextFormat = tf2;
			(m_AppState.skin.questionNumberWrapper.question_Number as TextField).embedFonts = true;
			(m_AppState.skin.questionNumberWrapper.question_Number as TextField).setTextFormat(tf2);
			
			FrameControlUtils.StopAtFrameN(m_AppState);
			
			m_AppState.GSOCheader.pagination.setNumMembers(SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount());
			m_AppState.GSOCheader.pagination.goToIndex(SessionInformation_GirlsTablet.TeamInfo.GetActiveIndex());
			m_AppState.GSOCheader.pagination.setActiveName(SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().Name);
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			setUpScreen(0);
			
			EndInit();
		}
		
		private function parseQuestions(w:int):void
		{
			var data:CareerDataModel = CareerDataUtils.careerDataModel;
			var ar:Array = new Array();
			for (var i:int = 0; i < data.getNumQuestions(); i++)
			{
				var tempArray:Array = new Array();
				var currentQuestion:QuestionDataModel = data.getQuestionAtIndex(i);
				tempArray.push(currentQuestion.getDataWithKey(QuestionDataModel.KEY_QUESTION_TEXT));
				for (var q:int = 0; q < currentQuestion.getNumAnswers(); q++)
				{	
					tempArray.push(currentQuestion.getAnswerAtIndex(q).getDataWithKey(AnswerDataModel.KEY_ANSWER_TEXT));
				}
				ar.push(tempArray);
			}
			
			switch (w)
			{
				case 1:
					this._questions1 = ar;
					break;
				case 2:
					this._questions2 = ar;
					break;
				case 3:
					this._questions3 = ar;
					break;
			}
		}
		
		private function setUpScreen(startIndex:int):void
		{
			m_AppState.skin.answerMechanism.addEventListener(InputController.CLICK, moveSlider, this, false);
			
			m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
			m_AppState.skin.back_Button.visible = false;
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				this.sliderDraggableObject = new DraggableObject(m_AppState.skin.answerMechanism.arrows_Select_Slider);
				this.sliderDraggableObject.AddEventListeners();
				this.sliderDraggableObject.addEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.sliderDraggableObject.addEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
			this._curSelection = 1;
			
			if (SessionInformation_GirlsTablet.Age != AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.save_Answer_Button.y = 1826;
			}
			
			var totAmount:int;
			
			switch (SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					totAmount = this._questions1.length;
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					totAmount = this._questions2.length;
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					totAmount = this._questions3.length;
					break;
			}
			var nextQ:int = Math.round((Math.random() * (totAmount - 3)) + 2);
			
			this._questionOrder[2] = nextQ;
			
			var lastQ:int = Math.round((Math.random() * (totAmount - 3)) + 2);
			
			while (lastQ == this._questionOrder[2])
			{
				lastQ = Math.round((Math.random() * (totAmount - 3)) + 2);
			}
			
			this._questionOrder[3] = lastQ;
			
			var last2Q:int = Math.round((Math.random() * (totAmount - 3)) + 2);
			
			while (last2Q == this._questionOrder[2] || last2Q == this._questionOrder[3])
			{
				last2Q = Math.round((Math.random() * (totAmount - 3)) + 2);
			}
			this._questionOrder[4] = last2Q;
			
			var last3Q:int = Math.round((Math.random() * (totAmount - 3)) + 2);
			
			while (last3Q == this._questionOrder[2] || last3Q == this._questionOrder[3] || last3Q == this._questionOrder[4])
			{
				last3Q = Math.round((Math.random() * (totAmount - 3)) + 2);
			}
			this._questionOrder[5] = last3Q;
		}
		
		private function NoteDragUpdateHandler(event:Event_DraggableObject):void
		{
			this.sliderDraggableObject.draggableObject.y = this.sliderDraggableObject.originalPosition.y;
			if (m_AppState.skin.answerMechanism.arrows_Select_Slider.x < 
				m_AppState.skin.answerMechanism.hitArea1.x)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea1.x;
			}
			if (m_AppState.skin.answerMechanism.arrows_Select_Slider.x >= m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width;
			}
			m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
		}
		
		private function NoteDragEndHandler(event:Event_DraggableObject):void
		{
			if (m_AppState.skin.answerMechanism.arrows_Select_Slider.x < 
				m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
				_curSelection = 0;
			}
			if (m_AppState.skin.answerMechanism.arrows_Select_Slider.x >= m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width
				&& m_AppState.skin.answerMechanism.arrows_Select_Slider.x < m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
				_curSelection = 1;
			}
			if (m_AppState.skin.answerMechanism.arrows_Select_Slider.x >= m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width - m_AppState.skin.answerMechanism.arrows_Select_Slider.width)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
				_curSelection = 2;
			}
			m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
		}
		
		private function getEventNameString(e:Event):String
		{
			var nameString:String = "";
			if(e is MouseEvent)
			{
				nameString = (e as MouseEvent).target.name;
			}
			
			if(e is TouchEvent)
			{
				nameString = (e as TouchEvent).target.name;
			}
			return nameString;
		}
		
		private function checkHitAreaVisible(nameString:String):Boolean
		{
			var isHitAreaVisible:Boolean = false;
			switch (nameString)
			{
				case "hitArea1":
					isHitAreaVisible = m_AppState.skin.answerMechanism.answerBox1.visible;
					break;
				case "hitArea2":
					isHitAreaVisible = m_AppState.skin.answerMechanism.answerBox2.visible;
					break;
				case "hitArea3":
					isHitAreaVisible = m_AppState.skin.answerMechanism.answerBox3.visible;
					break;
				case "hitArea4":
					isHitAreaVisible = m_AppState.skin.answerMechanism.answerBox4.visible;
					break;
			}
			return isHitAreaVisible;
		}
		
		private function moveSlider(e:Event):void
		{
			var nameString:String = getEventNameString(e);
			var isSeniorAmbassador:Boolean = SessionInformation_GirlsTablet.Age != AgeConstants.KEY_DAISYBROWNIE;
			var isHitAreaVisible:Boolean = isSeniorAmbassador ? checkHitAreaVisible(nameString): true;
			if(!isSeniorAmbassador && nameString == "hitArea4")
			{
				isHitAreaVisible = false;
			}
			if(isHitAreaVisible)
			{
				resolveHitArea(nameString);
			}
		}
		
		private function resolveHitArea(nameString:String):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_MULTIANSWERS[SessionInformation_GirlsTablet.Age]);
			switch (nameString)
			{
				case "hitArea1":
					animNextIn();
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(2);
						m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(1);
					}
					else
					{
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
					}
					m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
					_curSelection = 0;
					break;
				case "hitArea2":
					animNextIn();
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(2);
						m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(1);
					}
					else
					{
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
					}
					m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
					_curSelection = 1;
					break;
				case "hitArea3":
					animNextIn();
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(2);
						m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(1);
					}
					else
					{
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
					}
					m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
					_curSelection = 2;
					break;
				case "hitArea4":
					animNextIn();
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(1);
						m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(2);
					}
					m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
					_curSelection = 3;
					break;
			}
		}
		
		private function nextQuestion(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			if (SessionInformation_GirlsTablet.Age != AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.save_Answer_Button.removeEventListener(InputController.CLICK, nextQuestion);
			}
			if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(1);
				m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
				m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(1);
				m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(1);
			}
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
				m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
			}
			else
			{
				switch(this._curSelection)
				{
					case 0:
						m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(2);
						break;
					case 1:
						m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(2);
						break;
					case 2:
						m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(2);
						break;
					case 03:
						m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(2);
						break;
				}
			}
			
			this._answerArray[this._whichText] = this._curSelection;
			SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY, this._answerArray);
			switch(this._whichQuestion)
			{
				case 0:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q2";
					m_AppState.skin.back_Button.visible = true;
					m_AppState.skin.back_Button.addEventListener(InputController.CLICK, prevQuestion);
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[1]][0];

					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[1]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[1]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[1]][3].toUpperCase();
						m_AppState.skin.answerMechanism.answerField4.text = this._questions[this._questionOrder[1]][4].toUpperCase();
					}
					this._whichQuestion = 1;
					this._whichText = this._questionOrder[this._whichQuestion];
					break;
				case 1:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q3";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[2]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[2]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[2]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[2]][3].toUpperCase();
						m_AppState.skin.answerMechanism.answerBox4.visible = false;
						m_AppState.skin.answerMechanism.answerField4.visible = false; /****************/
					}
					this._whichQuestion = 2; 
					this._whichText = this._questionOrder[this._whichQuestion];
					break;
				case 2:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q4";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[3]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE) 
					{
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[3]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[3]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[3]][3].toUpperCase();
					}
					this._whichQuestion = 3;
					this._whichText = this._questionOrder[3];
					break;
				case 3:
					if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
					{
						SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY, this._answerArray);
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE);
					}
					else
					{
						m_AppState.skin.questionNumberWrapper.question_Number.text = "Q5";
						m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[4]][0];
						if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
						{
							m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[4]][1].toUpperCase();
							m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[4]][2].toUpperCase();
							m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[4]][3].toUpperCase();
						}
						this._whichQuestion = 4;
						this._whichText = this._questionOrder[4];
					}
					break;
				case 4:
					if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_JUNIORCADETTE)
					{
						SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY, this._answerArray);
						StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE);
					}
					else
					{
						m_AppState.skin.questionNumberWrapper.question_Number.text = "Q6";
						m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[5]][0];
						if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
						{
							m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[5]][1].toUpperCase();
							m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[5]][2].toUpperCase();
							m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[5]][3].toUpperCase();
						}
						this._whichQuestion = 5;
						this._whichText = this._questionOrder[5];
					}
					break;
				case 5:
					SessionInformation_GirlsTablet.TeamInfo.GetActiveGirlInformation().GirlUserData.SetData(GSOC_UserData_Girl_DataKeys.KEY_ANSWER_ARRAY, this._answerArray);
					StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_EXPLOREYOURPASSION, StateController_StateIDs_ExploreYourPassion.STATE_QUESTIONSDONE);
					break;
			}
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				var theWidth:int = m_AppState.skin.answerMechanism.answer_Question_Box.width - 100;
				switch(this._answerArray[this._whichText])
				{
					case 0:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
					case 1:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
					case 2:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
				}
			}
			else
			{
				setAnswerSelection();
			}
			
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
			}
			
			Utils_Text.FitTextToTextfield(m_AppState.skin.questionWrapper.question_TextField, 4);
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE) NoteDragEndHandler(new Event_DraggableObject(Event_DraggableObject.DRAG_END, new Point(0,0)));
			
			SessionInformation_GirlsTablet.UploadCurrentInformation();
		}
		
		private function setAnswerSelection():void
		{
			m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(1);
			m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(1);
			m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(1);
			m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(1);
			animNextOut();
			switch(this._answerArray[this._whichText])
			{
				case 0:
					m_AppState.skin.answerMechanism.answerBox1.gotoAndStop(2);
					animNextIn();
					break;
				case 1:
					m_AppState.skin.answerMechanism.answerBox2.gotoAndStop(2);
					animNextIn();
					break;
				case 2:
					m_AppState.skin.answerMechanism.answerBox3.gotoAndStop(2);
					animNextIn();
					break;
				case 3:
					m_AppState.skin.answerMechanism.answerBox4.gotoAndStop(2);
					animNextIn();
					break;
			}
		}
		
		private function animNextOut():void
		{
			m_AppState.skin.save_Answer_Button.removeEventListener(InputController.CLICK, nextQuestion);
			TweenMax.to(m_AppState.skin.save_Answer_Button, .5, {y:1826});
		}
		private function animNextIn():void
		{
			m_AppState.skin.save_Answer_Button.addEventListener(InputController.CLICK, nextQuestion);
			TweenMax.to(m_AppState.skin.save_Answer_Button, .5, {y:saveLocY});
		}
		
		private function prevQuestion(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			this._whichQuestion--;
			
			switch(this._whichQuestion)
			{
				case 0:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q1";
					m_AppState.skin.back_Button.visible = false;
					m_AppState.skin.back_Button.removeEventListener(InputController.CLICK, nextQuestion);
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[0]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE)
					{
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[0]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[0]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[0]][3].toUpperCase();
						m_AppState.skin.answerMechanism.answerField4.text = this._questions[this._questionOrder[0]][4].toUpperCase();
					}
					this._whichQuestion = 0;
					this._whichText = this._questionOrder[0];
					break;
				case 1:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q2";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[1]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE) {
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[1]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[1]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[1]][3].toUpperCase();
						m_AppState.skin.answerMechanism.answerField4.text = this._questions[this._questionOrder[1]][4].toUpperCase();
						m_AppState.skin.answerMechanism.answerBox4.visible = true;
						m_AppState.skin.answerMechanism.answerField4.visible = true;
					}
					this._whichQuestion = 1;
					this._whichText = this._questionOrder[1];
					break;
				case 2:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q3";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[2]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE) {
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[2]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[2]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[2]][3].toUpperCase();
					}
					this._whichQuestion = 2;
					this._whichText = this._questionOrder[2];
					break;
				case 3:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q4";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[3]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE) {
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[3]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[3]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[3]][3].toUpperCase();
					}
					this._whichQuestion = 3;
					this._whichText = this._questionOrder[3];
					break;
				case 4:
					m_AppState.skin.questionNumberWrapper.question_Number.text = "Q5";
					m_AppState.skin.questionWrapper.question_TextField.text = this._questions[this._questionOrder[4]][0];
					if (SessionInformation_GirlsTablet.Age > AgeConstants.KEY_DAISYBROWNIE) {
						m_AppState.skin.answerMechanism.answerField1.text = this._questions[this._questionOrder[4]][1].toUpperCase();
						m_AppState.skin.answerMechanism.answerField2.text = this._questions[this._questionOrder[4]][2].toUpperCase();
						m_AppState.skin.answerMechanism.answerField3.text = this._questions[this._questionOrder[4]][3].toUpperCase();
					}
					this._whichQuestion = 4;
					this._whichText = this._questionOrder[4];
					break;
			}
			
			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
				switch(this._answerArray[this._whichText])
				{
					case 0:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea1.x + m_AppState.skin.answerMechanism.hitArea1.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
					case 1:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea2.x + m_AppState.skin.answerMechanism.hitArea2.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
					case 2:
						m_AppState.skin.answerMechanism.arrows_Select_Slider.x = m_AppState.skin.answerMechanism.hitArea3.x + m_AppState.skin.answerMechanism.hitArea3.width/2 - m_AppState.skin.answerMechanism.arrows_Select_Slider.width/2;
						m_AppState.skin.answerMechanism.anchor_Select_Slider.x = m_AppState.skin.answerMechanism.arrows_Select_Slider.x;
						break;
				}
			}
			else
			{
				setAnswerSelection();
			}
			
			Utils_Text.FitTextToTextfield(m_AppState.skin.questionWrapper.question_TextField, 4);
		}
		
		protected override function DeInit():void
		{
			m_AppState.skin.save_Answer_Button.removeEventListener(InputController.CLICK, nextQuestion);
			m_AppState.skin.back_Button.removeEventListener(InputController.CLICK, prevQuestion);
			m_AppState.skin.answerMechanism.removeEventListener(InputController.CLICK, moveSlider);

			if (SessionInformation_GirlsTablet.Age == AgeConstants.KEY_DAISYBROWNIE)
			{
				this.sliderDraggableObject.RemoveEventListeners();
				this.sliderDraggableObject.removeEventListener(Event_DraggableObject.DRAG_UPDATE, this.NoteDragUpdateHandler);
				this.sliderDraggableObject.removeEventListener(Event_DraggableObject.DRAG_END, this.NoteDragEndHandler);
			}
			
			CitrusFramework.contentLayer.removeChild(m_AppState);
			EndDeInit();
		}
	}
}
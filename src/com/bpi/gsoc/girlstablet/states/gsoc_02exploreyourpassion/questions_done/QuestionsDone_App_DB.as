package com.bpi.gsoc.girlstablet.states.gsoc_02exploreyourpassion.questions_done
{
	import com.bpi.gsoc.components.exploreyourpassion.QuestionsDone_DB_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;

	public class QuestionsDone_App_DB extends QuestionsDone_App
	{
		public function QuestionsDone_App_DB()
		{
			m_Skin = new QuestionsDone_DB_Skin();
			//this.addChild(m_Skin);
			//m_Pagination = new Pagination(new Pagination_DB(), AgeFontLinkages.DB_HEADER_FONT);
			m_HeaderFont = AgeFontLinkages.DB_HEADER_FONT;
			m_BodyFont = AgeFontLinkages.DB_BODY_FONT;
			super.Initialize();
		}
	}
}
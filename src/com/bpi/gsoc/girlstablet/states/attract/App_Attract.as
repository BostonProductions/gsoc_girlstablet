package com.bpi.gsoc.girlstablet.states.attract
{
	import com.bpi.citrusas.images.AnimationClip;
	import com.bpi.citrusas.input.singleinput.Input;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.gsoc.components.attractvideos.Attract_Connected;
	import com.bpi.gsoc.components.attractvideos.Attract_Disconnected;
	import com.bpi.gsoc.components.girlstablet.general.Attract_Background;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	
	import flash.display.Sprite;
	import flash.utils.ByteArray;

	public class App_Attract extends Sprite
	{
		private var m_Background:AnimationClip;
		
		private var m_Attract:AnimationClip;
		private var m_Attract_IsConnected:Boolean;
		
		public function App_Attract()
		{
			m_Background = new AnimationClip(Attract_Background);
			
			m_Attract = new AnimationClip();
		}
		
		public function InitializeEventListeners():void
		{
		}
		
		public function Initialize():void
		{	
			this.addChild(m_Background);
		}
		
		public function CleanUp():void
		{
		}
		
		public function Init():void
		{
			refreshAttract();
		}
		
		public function DeInit():void
		{
			m_Attract.Stop();
		}
		
		public function Update():void
		{
			checkAttract();
			checkServerConnection();
		}
		
		public function QueuedUpdate():void
		{
			
		}
		
		public function Resume():void
		{
		}
		
		public function Pause():void
		{
			
		}
		
		private function refreshAttract():void
		{
			var attractClass:Class = m_Attract_IsConnected ? Attract_Connected : Attract_Disconnected;
			if(this.contains(m_Attract))
			{
				this.removeChild(m_Attract);
			}
			m_Attract = new AnimationClip(attractClass);
			m_Attract.Play();
			this.addChild(m_Attract);
		}
		
		private function checkAttract():void
		{
			if(m_Attract_IsConnected != Client.IsConnected)
			{
				m_Attract_IsConnected = Client.IsConnected;
				refreshAttract();
			}
		}
		
		private function checkServerConnection():void
		{
			if(Client.IsConnected && Input.IsMouseClicked)
			{
				var networkMessageByteArray:ByteArray = new ByteArray();
				networkMessageByteArray.writeInt(SessionInformation_GirlsTablet.TabletIndex);
				Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_TABLET_JOINED, networkMessageByteArray));
			}
		}
	}
}
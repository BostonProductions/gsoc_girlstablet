package com.bpi.gsoc.girlstablet.states.gsoc_09mindmap.classes
{
	import com.bpi.gsoc.framework.gsocui.SkillMap.Shapes.SkillShape;
	
	import flash.events.Event;

	public class Event_StarChanged extends Event
	{
		public static const STAR_CHANGED:String = "STAR_CHANGED";
		
		public var e_StarPlaced:Boolean;
		public var e_SkillShape:SkillShape;
		
		public function Event_StarChanged(type:String, starPlaced:Boolean, skillShape:SkillShape)
		{
			super(type);
			
			e_StarPlaced = starPlaced;
			e_SkillShape = skillShape;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection
{
	import com.bpi.gsoc.components.pictureyourself.NameSelection_SA_Skin;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;

	public class NameSelection_App_SA extends NameSelection_Skin
	{
		public function NameSelection_App_SA()
		{
			m_Skin = new NameSelection_SA_Skin();
			m_HeaderFont = AgeFontLinkages.SA_HEADER_FONT;
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			
			super.Initialize();
		}
	}
}
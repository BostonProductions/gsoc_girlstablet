package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection
{
	import com.adobe.utils.StringUtil;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.ButtonPlacement;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.GirlInformation;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Girl_DataKeys;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.gsocui.keyboards.GSOC_Keyboards;
	import com.bpi.gsoc.framework.networking.ScoutManager;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.global_data.FrameControlUtils;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.PictureYourself_AppState;
	import com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.name_selection.classes.TextField_NameEntry;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_PictureYourself;
	
	import flash.desktop.NativeApplication;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	public class NameSelection_State extends PictureYourself_AppState
	{
		private var m_AppState:NameSelection_Skin;
		private var _vect_numberButtons:Vector.<MovieClip>;
		private var _vect_nameFields:Vector.<TextField_NameEntry>;
		private var _activeTextField:int;
		private var _numParticipants:int;
		private var isEnter:Boolean = false;
		private var shiftDown:Boolean = false;
		
		public function NameSelection_State()
		{
			super(StateController_StateIDs_PictureYourself.STATE_NAMESELECTION);
		}
		
		public override function Initialize():void
		{
		}
		
		protected override function Init():void
		{
			resetInformation();
			
			TabletCamera.Init();
			
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_AppState = new NameSelection_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_AppState = new NameSelection_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_AppState = new NameSelection_App_SA();
					break;
				default:
					m_AppState = new NameSelection_App_DB();	
			}
			
			GSOC_Keyboards.SetKeyboardType(GSOC_Keyboards.KEYBOARDTYPE_UPPERCASE);
			m_AppState.Init();
			
			this._vect_nameFields = new Vector.<TextField_NameEntry>();
			this._vect_numberButtons = new Vector.<MovieClip>();
			this._activeTextField = -1;
			this._numParticipants = -1;	
			
			
			this._vect_numberButtons.push(
				m_AppState.skin.teamButton1,
				m_AppState.skin.teamButton2,
				m_AppState.skin.teamButton3,
				m_AppState.skin.teamButton4,
				m_AppState.skin.teamButton5,
				m_AppState.skin.teamButton6,
				m_AppState.skin.teamButton7,
				m_AppState.skin.teamButton8
			);
			
			this._vect_nameFields.push(
				new TextField_NameEntry(m_AppState.skin.nameTextBox1, m_AppState.skin, m_AppState.headerFont, 1),
				new TextField_NameEntry(m_AppState.skin.nameTextBox2, m_AppState.skin, m_AppState.headerFont, 2),
				new TextField_NameEntry(m_AppState.skin.nameTextBox3, m_AppState.skin, m_AppState.headerFont, 3),
				new TextField_NameEntry(m_AppState.skin.nameTextBox4, m_AppState.skin, m_AppState.headerFont, 4),
				new TextField_NameEntry(m_AppState.skin.nameTextBox5, m_AppState.skin, m_AppState.headerFont, 5),
				new TextField_NameEntry(m_AppState.skin.nameTextBox6, m_AppState.skin, m_AppState.headerFont, 6),
				new TextField_NameEntry(m_AppState.skin.nameTextBox7, m_AppState.skin, m_AppState.headerFont, 7),
				new TextField_NameEntry(m_AppState.skin.nameTextBox8, m_AppState.skin, m_AppState.headerFont, 8)
			);
			
			this.setUpButtons();
			this.addEventListeners();
			this.setUpTextFields();
			
			m_AppState.skin.teamNamesText.visible = false;
			
			GSOC_ButtonToggle.ToggleButton(false, this.m_AppState.skin.save_button, this.enableSaveButton, this.disableSaveButton);
			this.m_AppState.skin.save_button.x = ButtonPlacement.NEXT_X;
			
			FrameControlUtils.StopAtFrameN(m_AppState);
			
			if(SessionInformation_GirlsTablet.Track == TrackConstants.TAKE_ACTION)
			{
				m_AppState.skin.header.gotoAndStop(2);
				if(SessionInformation_GirlsTablet.Age != AgeConstants.KEY_JUNIORCADETTE)
					m_AppState.GSOCheader.resetLocations(m_AppState.skin.header.paginationArea.x, m_AppState.skin.header.paginationArea.width, m_AppState.skin.header.paginationArea.height);
				else
					m_AppState.GSOCheader.resetLocations(m_AppState.skin.header.paginationArea.x - 100, m_AppState.skin.header.paginationArea.width, m_AppState.skin.header.paginationArea.height);
			}
			else
			{
				m_AppState.skin.header.gotoAndStop(1);
			}
			
			CitrusFramework.contentLayer.addChild(m_AppState);
			
			EndInit();
		}
		
		protected override function DeInit():void
		{	
			ScoutManager.stopProcess();
			
			isEnter = false;
			shiftDown = false;
			this.removeEventListeners();
			CitrusFramework.contentLayer.removeChild(m_AppState);
			
			super.DeInit();
		}
		
		private function resetInformation():void
		{
			var girlInfo:Vector.<GirlInformation> = SessionInformation_GirlsTablet.TeamInfo.GetGirlInformationList();
			
			for(var i:int = 0; i < girlInfo.length; i++)
			{
				girlInfo[i].GirlUserData.userData[GSOC_UserData_Girl_DataKeys.KEY_AVATAR_COMPLETE] = false;
			}
			
			SessionInformation_GirlsTablet.UploadCurrentInformation();
		}
		
		private function setUpButtons():void
		{
			this.clearButtonStates();
		}
		
		private function addEventListeners():void
		{
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, this.receiveChar);
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_UP, this.keyUp);
			TextField_NameEntry.TextEntryDispatcher.addEventListener(TextField_NameEntry.TEXT_ENTERED, this.checkFilledTextFields);
			
			for (var i:int = 0; i < this._vect_numberButtons.length; i++) 
			{
				this._vect_numberButtons[i].addEventListener(InputController.CLICK, this.setNumParticipants);
			}
			
			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{
				field.addEventListener(InputController.CLICK, this.setActiveTextFieldByMouseClick);
			}			
		}
		
		private function removeEventListeners():void
		{	
			ScoutManager.ScoutManagerEventDispatcher.removeEventListener(ScoutManager.EVENT_SCOUTS_RESET_COMPLETE, eh_ScoutsResetComplete);
			ScoutManager.ScoutManagerEventDispatcher.removeEventListener(ScoutManager.EVENT_SCOUTS_CREATE_COMPLETE, eh_ScoutsCreateComplete);
			
			NativeApplication.nativeApplication.removeEventListener(KeyboardEvent.KEY_DOWN, this.receiveChar);
			NativeApplication.nativeApplication.removeEventListener(KeyboardEvent.KEY_UP, this.keyUp);
			TextField_NameEntry.TextEntryDispatcher.removeEventListener(TextField_NameEntry.TEXT_ENTERED, this.checkFilledTextFields);
			
			for (var i:int = 0; i < this._vect_numberButtons.length; i++)
			{
				this._vect_numberButtons[i].removeEventListener(InputController.CLICK, this.setNumParticipants);
			}
			
			for each (var field:TextField_NameEntry in this._vect_nameFields)
			{
				field.removeEventListener(InputController.CLICK, this.setActiveTextFieldByMouseClick);
			}
		}
		
		private function eh_ReactivateCurrentTextField(e:Event):void
		{
			if(this._activeTextField >= 0)
			{
				this.activateSingleTextField(this._activeTextField);
			}
		}
		
		private function receiveChar(e:KeyboardEvent):void
		{
			if(this._activeTextField >= 0)
			{
				switch (e.keyCode)
				{
					case Keyboard.SHIFT:
						shiftDown = true;
						break;
					case Keyboard.TAB:
						if(shiftDown)
						{
							if(this._activeTextField == 0)
							{
								this._activeTextField = (this._numParticipants - 1);
							}
							else
							{
								this._activeTextField = (this._activeTextField - 1);
							}
						}
						else
						{
							this._activeTextField = (this._activeTextField + 1) % this._numParticipants;
						}
						
						if (isEnter)
						{
							isEnter = false;
							this.activateSingleTextField(this._activeTextField);
						}
						return;
					case Keyboard.ENTER:
						isEnter = true;
						var keyCode:uint = Keyboard.TAB;
						NativeApplication.nativeApplication.dispatchEvent(new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, keyCode, keyCode));
						return;
					case Keyboard.ESCAPE:
						return;
				}
			}
		}
		
		private function keyUp(e:KeyboardEvent):void
		{
			switch (e.keyCode)
			{
				case Keyboard.SHIFT:
					shiftDown = false;
					break;
			}
		}
		
		private function setActiveTextFieldByMouseClick(e:Event = null):void
		{
			this._activeTextField = this._vect_nameFields.indexOf(e.currentTarget as TextField_NameEntry);
			CitrusFramework.frameworkStage.focus = (e.currentTarget as TextField_NameEntry).textField;
			this.activateSingleTextField(this._activeTextField);
		}
		
		private function setUpTextFields():void
		{
			this._numParticipants = 0;
			Utils_Text.ConvertToEmbeddedFont(m_AppState.skin.questionText.textField, GSOC_Info.BodyFont, false);
			Utils_Text.ConvertToEmbeddedFont(m_AppState.skin.teamNamesText.textField, GSOC_Info.BodyFont, false);
			
			Utils_Text.FitTextToTextfield(m_AppState.skin.questionText.textField);
			Utils_Text.FitTextToTextfield(m_AppState.skin.teamNamesText.textField);

			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{
				field.clearField();
				field.hide();
				field.eh_Unhighlight();
			}
			
			this._activeTextField = -1;
		}
		
	
		private function clearButtonStates():void
		{
			for each (var btn:MovieClip in this._vect_numberButtons) 
			{
				btn.gotoAndStop(1);
			}
		}
		
		private function resetGraphics():void 
		{
			this.clearButtonStates();
			
			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{
				field.visible = true;
			}
		}
		
		private function checkFilledTextFields(e:Event = null):void 
		{
			var numFilledFields:int = 0;
			
			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{	
				var isTextFieldFilled:Boolean = StringUtil.trim(field.getText());
				numFilledFields = isTextFieldFilled ? numFilledFields + 1 : numFilledFields;
			}
			
			var isSaveButtonActive:Boolean = (numFilledFields == this._numParticipants);
			GSOC_ButtonToggle.ToggleButton(isSaveButtonActive, this.m_AppState.skin.save_button, this.enableSaveButton, this.disableSaveButton);
		}
		
		private function enableSaveButton():void
		{
			m_AppState.skin.save_button.addEventListener(InputController.CLICK, this.nextHandler);
		}
		
		private function disableSaveButton():void
		{
			m_AppState.skin.save_button.removeEventListener(InputController.CLICK, this.nextHandler);
		}
		
		private function setNumParticipants(e:Event):void 
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTNUMBERS[SessionInformation_GirlsTablet.Age]);
			var numFields:int = this._vect_numberButtons.indexOf(e.target as MovieClip);
			var i:int = 0;
			this.clearButtonStates();
			this._vect_numberButtons[numFields].gotoAndStop(2);
			this._numParticipants = numFields+1;
			
			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{
				if (i <= this._numParticipants - 1) 
				{
					field.show();
				}
				else 
				{
					field.clearField();
					field.hide();
				}
				i++;
			}
			
			// activate the first tf if it's the first selection or if the active tf was hidden by the most recent selection
			if(!this.m_AppState.skin.teamNamesText.visible || this._activeTextField + 1 > this._numParticipants)
			{
				this._activeTextField = 0;
				this.activateSingleTextField(0);
			}
			else
			{
				CitrusFramework.frameworkStage.focus = this._vect_nameFields[this._activeTextField].textField;
			}
			
			this.m_AppState.skin.teamNamesText.visible = true;
			this.checkFilledTextFields();
		}
		
		private function activateSingleTextField(fieldNum:int):void
		{
			for each (var field:TextField_NameEntry in this._vect_nameFields) 
			{
				field.eh_Unhighlight();
			}
			
			this._vect_nameFields[fieldNum].eh_Highlight();
			CitrusFramework.frameworkStage.focus = this._vect_nameFields[fieldNum].textField;
		}
		
		private function nextHandler(e:Event):void 
		{
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_PictureYourself.STATE_LOADING, true);
			
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			// clean up listeners!!
			for each (var btn:MovieClip in this._vect_numberButtons) 
			{
				btn.removeEventListener(InputController.CLICK, this.setNumParticipants);
			}
			
			for each(var field:TextField_NameEntry in this._vect_nameFields) 
			{
				field.eh_Unhighlight();
			}
			
			// we need to delete all the previous scouts that were created (INCLUDING ANY IMAGES)
			ScoutManager.ScoutManagerEventDispatcher.addEventListener(ScoutManager.EVENT_SCOUTS_RESET_COMPLETE, eh_ScoutsResetComplete);
			ScoutManager.resetScouts();
		}
		
		private function eh_ScoutsResetComplete(evt:Event):void
		{
			ScoutManager.ScoutManagerEventDispatcher.removeEventListener(ScoutManager.EVENT_SCOUTS_RESET_COMPLETE, eh_ScoutsResetComplete);
			
			SessionInformation_GirlsTablet.TeamInfo.Clear();
			
			var scoutNames:Vector.<String> = new Vector.<String>();
			// set girls names into array!
			for each (var field:TextField_NameEntry in this._vect_nameFields)
			{
				if(field.isShown())
				{
					scoutNames.push(field.getText());
				}
			}
			
			ScoutManager.ScoutManagerEventDispatcher.addEventListener(ScoutManager.EVENT_SCOUTS_CREATE_COMPLETE, eh_ScoutsCreateComplete);
			ScoutManager.createScouts(scoutNames);
		}
		
		private function eh_ScoutsCreateComplete(evt:Event):void
		{
			ScoutManager.ScoutManagerEventDispatcher.removeEventListener(ScoutManager.EVENT_SCOUTS_CREATE_COMPLETE, eh_ScoutsCreateComplete);
			StateControllerManager.Pop(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM);
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM, StateController_StateIDs_PictureYourself.STATE_TAKEPICTURE);
			SessionInformation_GirlsTablet.UploadCurrentInformation();
		}
	}
}
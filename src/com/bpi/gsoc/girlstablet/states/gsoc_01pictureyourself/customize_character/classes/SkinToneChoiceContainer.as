package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes
{
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	
	public class SkinToneChoiceContainer extends EventDispatcher
	{
		private var m_SkinTones:Vector.<SkinToneChoiceBox>;
		
		public function SkinToneChoiceContainer()
		{
			super();
			m_SkinTones = new Vector.<SkinToneChoiceBox>();
		}
		
		public function AddSkinColor(referenceSprite:Sprite, fillColor:uint, borderColor:uint, glowColor:uint, shape:String):void
		{
			var newSelection:SkinToneChoiceBox = new SkinToneChoiceBox(referenceSprite, fillColor, borderColor, glowColor, shape);
			newSelection.addEventListener(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, this.eh_SkinColorSelected);
			m_SkinTones.push(newSelection);
		}
		
		public function Destroy():void
		{
			for each(var box:SkinToneChoiceBox in m_SkinTones)
			{
				box.Destroy();
				box.removeEventListener(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, this.eh_SkinColorSelected);
			}
			m_SkinTones = null;
		}
		
		private function clearAllSelections():void
		{
			for each(var box:SkinToneChoiceBox in m_SkinTones)
			{
				box.UnGlow();
			}
		}
		
		private function eh_SkinColorSelected(e:Event_SkinColorChoice):void
		{
			this.dispatchEvent(new Event_SkinColorChoice(Event_SkinColorChoice.SKIN_COLOR_CHOSEN, e.color, e.sender));
			this.clearAllSelections();
			e.sender.Glow();
		}
	}
}
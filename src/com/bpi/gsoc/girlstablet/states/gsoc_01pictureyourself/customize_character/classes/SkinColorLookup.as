package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself.customize_character.classes
{
	public class SkinColorLookup
	{
		private static var ms_Colors_DB:Vector.<uint>;
		private static var ms_Colors_JC:Vector.<uint>;
		private static var ms_Colors_SA:Vector.<uint>;
		
		private static var ms_NumDBColors:int = 0;
		private static var ms_NumJCColors:int = 0;
		private static var ms_NumSAColors:int = 0;
		
		private static var ms_GeneralNumColors:int = 0;
	
		public static function Initialize():void
		{
			ms_Colors_DB = new Vector.<uint>();
			ms_Colors_JC = new Vector.<uint>();
			ms_Colors_SA = new Vector.<uint>();
			
			ms_Colors_DB.push(0xffd5c0);
			ms_Colors_DB.push(0xffe5ce);
			ms_Colors_DB.push(0xfef1c3);
			ms_Colors_DB.push(0xbe9672);
			ms_Colors_DB.push(0x886b51);
			ms_Colors_DB.push(0x60462a);
			ms_Colors_DB.push(0xf6c4c4);
			ms_Colors_DB.push(0xfecb31);
			ms_Colors_DB.push(0x993092);
			ms_Colors_DB.push(0xe21567);
			
			ms_Colors_JC.push(0xffd5c0);
			ms_Colors_JC.push(0xffe5ce);
			ms_Colors_JC.push(0xfef1c3);
			ms_Colors_JC.push(0xbe9672);
			ms_Colors_JC.push(0x886b51);
			ms_Colors_JC.push(0x60462a);
			ms_Colors_JC.push(0xf6c4c4);
			ms_Colors_JC.push(0xfecb31);
			ms_Colors_JC.push(0x0cb1a5);
			ms_Colors_JC.push(0xb4d335);
			
			ms_Colors_SA.push(0xffd5c0);
			ms_Colors_SA.push(0xffe5ce);
			ms_Colors_SA.push(0xfef1c3);
			ms_Colors_SA.push(0xbe9672);
			ms_Colors_SA.push(0x886b51);
			ms_Colors_SA.push(0x60462a);
			ms_Colors_SA.push(0xf6c4c4);
			ms_Colors_SA.push(0xfecb31);
			ms_Colors_SA.push(0x6bcbd9);
			ms_Colors_SA.push(0x9d91c6);		
		
			ms_NumDBColors = ms_Colors_DB.length;
			ms_NumJCColors = ms_Colors_JC.length;
			ms_NumSAColors = ms_Colors_SA.length;
			ms_GeneralNumColors = ms_NumDBColors;
		}
		
		public static function GetDBColorAtIndex(i:int):uint
		{
			if(ms_GeneralNumColors == 0) Initialize();
			return(ms_Colors_DB[i]);
		}
		
		public static function GetJCColorAtIndex(i:int):uint
		{
			if(ms_GeneralNumColors == 0) Initialize();
			return(ms_Colors_JC[i]);
		}
		
		public static function GetSAColorAtIndex(i:int):uint
		{
			if(ms_GeneralNumColors == 0) Initialize();
			return(ms_Colors_SA[i]);
		}
		
		public static function get NumColors():int
		{
			if(ms_GeneralNumColors == 0) Initialize();
			return ms_GeneralNumColors;
		}
		
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_01pictureyourself
{
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;

	public class PictureYourself_AppState extends GSOC_AppState
	{
		public function PictureYourself_AppState(stateID:String)
		{
			super(stateID);
		}
		
		protected override function DeInit():void
		{
			var isInCareerTrack:Boolean = SessionInformation_GirlsTablet.Track == TrackConstants.CAREER;
			var scID:int = isInCareerTrack ? StateController_IDs.STATECONTROLLER_PICTUREYOURSELF : StateController_IDs.STATECONTROLLER_ASSEMBLEYOURTEAM;
			var isLeavingActivity:Boolean = StateControllerManager.GetStateController(scID).CheckIfMovingToEmptyState();
			
			if(isLeavingActivity)
			{
				TabletCamera.DeInit();
			}
			
			super.DeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.messaging
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	public class VideoSaveBackground extends MovieClip
	{
		private const BKGD_DIMS:Point = new Point(800, 450);
		public function VideoSaveBackground()
		{
			super();
			
			this.graphics.beginFill(0xeeeeee, 0.9);
			this.graphics.drawRect(0, 0, BKGD_DIMS.x, BKGD_DIMS.y);
			this.graphics.endFill();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.reviewvideo.videopanel.progressbar.events
{
	import flash.events.Event;
	
	public class Event_ProgressBar extends Event
	{
		public static const EVENT_SET_POSITION:String = "EVENT_SET_POSITION";
		
		private var m_Percentage:Number;
		
		public function Event_ProgressBar(type:String, percentage:Number) 
		{
			super(type, false, false);
			
			m_Percentage = percentage;
		}
		
		public function get E_Percentage():Number
		{
			return m_Percentage;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.playbackvideo
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.video.InteractiveVideo;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class PlayBackVideo_State extends GSOC_AppState
	{
		private var m_App:PlayBackVideo_App;
		private var instText:TextField;
		private var nextButton:MovieClip
		private var vidPlayer:InteractiveVideo;
		
		public function PlayBackVideo_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_PLAYBACKVIDEO);
		}
		
		protected override function Init():void
		{	
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new PlayBackVideo_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new PlayBackVideo_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new PlayBackVideo_App_SA();
					break;
			}
			
			CitrusFramework.contentLayer.addChild(m_App);
			
			m_App.Initialize();	
		}
		
		protected override function DeInit():void
		{		
			CitrusFramework.contentLayer.removeChild(m_App);
			m_App = null;
			
			super.EndDeInit();
		}
	}
}
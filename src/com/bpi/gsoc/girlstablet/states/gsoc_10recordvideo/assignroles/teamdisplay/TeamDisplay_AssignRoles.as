package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.teamdisplay
{
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_BPI;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.gsoc.framework.avatar.GSOC_Avatar_Skin;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.TeamDisplay;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.AssignRoles_App;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.events.Event_RoleIconDropped;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.DraggableRoleIcon;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.target.TargetBox;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class TeamDisplay_AssignRoles extends TeamDisplay
	{
		private var m_SelectedRoles:SelectedRolesDisplay;
		
		public static const ROLE_ASSIGNMENT_COMPLETE:String = "ROLE ASSIGNMENT COMPLETE";
		public static const ROLE_ASSIGNMENT_INCOMPLETE:String = "ROLE ASSIGNMENT INCOMPLETE";
		
		public function TeamDisplay_AssignRoles(maxDisplaySize:Point, roleTargetDisplay:SelectedRolesDisplay, showGirlNames:Boolean=true, spacer = 0)
		{
			super(maxDisplaySize, showGirlNames, spacer);
	
			m_SelectedRoles = roleTargetDisplay;
			DraggableRoleIcon.DraggableRoleEventDispatcher.addEventListener(Event_RoleIconDropped.FREE_ROLE_ICON_DROPPED, this.eh_FreeIconDropped);
			DraggableRoleIcon.DraggableRoleEventDispatcher.addEventListener(Event_RoleIconDropped.CAPTURED_ROLE_ICON_DROPPED, this.eh_CapturedIconDropped);
		}
		
		override public function AddAvatar(avatar:GSOC_Avatar_Skin, name:String="TEMP", color:uint = 0):void
		{
			super.AddAvatar(avatar, name);
		}
		
		public function PositionIconTargetBoxes():void
		{
			var lowestY:Number = 0;
			for(var i:int = 0; i < this.items.length; i++)
			{
//				var globalPt:Point = this.items[i].localToGlobal(new Point());
				var associatedAvatar:GSOC_Avatar_Skin = this.GetAvatarAtIndex(i);
				var globalPt:Point = associatedAvatar.localToGlobal(new Point());
				
				var associatedRoleBox:TargetBox = m_SelectedRoles.GetRoleBoxAt(i);

				associatedRoleBox.x = globalPt.x + associatedAvatar.width / 2 * this.scaleX - associatedRoleBox.width / 2;
				var tempLowest:Number = globalPt.y + associatedAvatar.height *  this.scaleY - associatedRoleBox.height - this.GetAddedHeightForAvatarAtIndex(i)/2 *this.scaleY;
				if(tempLowest > lowestY)
				{
					lowestY = tempLowest;
				}
				// to keep the boxes lined up correctly, instead of setting the y of each box, we'll keep the boxes at 0 and set the parent (the m_SelectedRoles sprite) to the lowest y position required
				//associatedRoleBox.y = globalPt.y + associatedAvatar.height *  this.scaleY - associatedRoleBox.height - this.GetAddedHeightForAvatarAtIndex(i)/2 *this.scaleY;
			}
			m_SelectedRoles.y = lowestY;
		}
		
		public function ExpandIconHitTargets():void	
		{
			for(var i:int = 0; i < this.items.length; i++)
			{			
				var associatedRoleBox:TargetBox = m_SelectedRoles.GetRoleBoxAt(i);
				var verticalSpacer:Number = AssignRoles_App.VERTICAL_CONTAINER_SPACER;
				var newHitArea:Sprite = new Sprite();
				newHitArea.graphics.beginFill(0x000000, 0);
				// draw new hit area so it covers the icon boxes below the avatars as well as the avatars themselves
				newHitArea.graphics.drawRect(0, 0, this.items[i].width, this.items[i].height+ (verticalSpacer + associatedRoleBox.height) * 1/this.scaleX);
				newHitArea.graphics.endFill();
				this.items[i].addChild(newHitArea);
				(this.items[i] as GSOC_Avatar_Skin).hitArea = newHitArea;
			}
		}
		
		private function checkForHit(mc:DisplayObject):Vector.<GSOC_Avatar_Skin>
		{
			var avatarsHit:Vector.<GSOC_Avatar_Skin> = new Vector.<GSOC_Avatar_Skin>();
			for each (var avatar:GSOC_Avatar_Skin in m_Items )
			{
			
				if(avatar.hitTestObject(mc))
				{
					avatarsHit.push(avatar);
				}
	
			}
	
			return avatarsHit;
		}
		
		public function eh_FreeIconDropped(e:Event_RoleIconDropped):void
		{
			var hitTargets:Vector.<GSOC_Avatar_Skin> = this.checkForHit(e.roleIcon.draggableObject);
			if(hitTargets.length != 0)
			{
				SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTROLES[SessionInformation_GirlsTablet.Age]);
				var actualTarget:GSOC_Avatar_Skin;
				
				// of all the avatars that were hit, select the one that overlaps with the icon the most
				var overlappingAreas:Array = new Array();
				for (var i:int = 0; i < hitTargets.length; i++)
				{
					var overlappingArea:Rectangle = Utils_BPI.GetOverlappingArea(hitTargets[i], e.roleIcon.draggableObject);
					overlappingAreas.push(overlappingArea.width * overlappingArea.height);
				}
				
				var max_number:Number = overlappingAreas[0];
				
				for( var j:int = 1; j < overlappingAreas.length; j++ )
				{
					max_number = Math.max( max_number, overlappingAreas[j] );
				}
				
				actualTarget = hitTargets[overlappingAreas.indexOf(max_number)];

				//get index of target
				m_SelectedRoles.GetRoleBoxAt(m_Items.indexOf(actualTarget)).AddItem(e.roleIcon);
					
				this.dispatchCompleteOrIncomplete();
			}
			else
			{
				// fly back to start? disappear? both!
				SoundHandler.PlaySound(GSOC_SFX.SOUND_DROPROLES[SessionInformation_GirlsTablet.Age]);
				e.roleIcon.FlyToStartAndDisappear();
			}
		}
		
		public function eh_CapturedIconDropped(e:Event_RoleIconDropped):void
		{
			this.dispatchCompleteOrIncomplete();
		}
		
		public function dispatchCompleteOrIncomplete():void
		{
			if(IsRoleAssignmentComplete())
			{
				this.dispatchEvent(new Event(ROLE_ASSIGNMENT_COMPLETE));
			}
			else
			{
				this.dispatchEvent(new Event(ROLE_ASSIGNMENT_INCOMPLETE));
			}
		}
		
		public function IsRoleAssignmentComplete():Boolean
		{
			var arr:Array = new Array();
			for (var i:int = 0; i < SessionInformation_GirlsTablet.TeamInfo.GetTeamMemberCount(); i++)
			{
				arr.push( m_SelectedRoles.GetRoleBoxAt(i).IsNotEmpty());
			}
			return arr.indexOf(false) == -1
		}
		
		public function GetRoleVectorForGirlAtIndex(i:int):Vector.<String>
		{
			return m_SelectedRoles.GetRoleBoxAt(i).GetRolesVector();
		}
		
		public override function DeInit():void
		{
			m_SelectedRoles.DeInit();
			DraggableRoleIcon.DraggableRoleEventDispatcher.removeEventListener(Event_RoleIconDropped.FREE_ROLE_ICON_DROPPED, this.eh_FreeIconDropped);
			DraggableRoleIcon.DraggableRoleEventDispatcher.removeEventListener(Event_RoleIconDropped.CAPTURED_ROLE_ICON_DROPPED, this.eh_CapturedIconDropped);
			super.DeInit();
		}	
	}
}
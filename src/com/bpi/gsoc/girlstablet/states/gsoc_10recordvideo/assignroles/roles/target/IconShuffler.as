package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.target
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Power3;
	
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class IconShuffler
	{ 
		private static var ms_TargetArea:Rectangle;
		private static var ms_Ease:*;
		private static var ms_Spacer:int;
	
		private static const ms_TWEEN_DURATION:Number = 0.5;
		private static const ms_PERCENT_AVAILABLE_SPACE:Number = 0.8;
		
		public static var ShuffleDispatcher:EventDispatcher = new EventDispatcher();
		public static const TWEEN_COMPLETE:String = "TWEEN COMPLETE";
		
		public static function Init(targetArea:Rectangle, spacer:int):void
		{
			ms_TargetArea = targetArea;
			ms_Spacer = 10;
			ShuffleDispatcher = new EventDispatcher();
			ms_Ease = Power3.easeOut;
		}
		
		public static function MoveToIconConfiguration(icons:Vector.<Sprite>, callback:Function):void
		{
			for each(var icon:Sprite in icons) TweenLite.killTweensOf(icons);
			var numIcons:int = icons.length;

			switch(numIcons)
			{
				case 1:
					moveToOneIcon(icons, callback);
					break;
				case 2:
					moveToTwoIcons(icons, callback);
					break;
				case 3:
					moveToThreeIcons(icons, callback);
					break;
				case 4:
					moveToFourIcons(icons, callback);
					break;
				case 5:
					moveToFiveIcons(icons, callback);
					break;
			}
		}
		
		protected static function moveToOneIcon(icons:Vector.<Sprite>, callback:Function):void
		{
			var targetPos:Point;
			
			var targetDims:Point = calculateScaledDimensions(new Point(ms_TargetArea.width * ms_PERCENT_AVAILABLE_SPACE, ms_TargetArea.height * ms_PERCENT_AVAILABLE_SPACE), icons[0]);
			targetPos = new Point(ms_TargetArea.width / 2 - targetDims.x / 2, ms_TargetArea.height / 2 - targetDims.y / 2);
			TweenLite.to(icons[0], ms_TWEEN_DURATION, {x:targetPos.x, y:targetPos.y, width:targetDims.x, height:targetDims.y, onComplete:callback, ease:ms_Ease})
		}
		
		protected static function moveToTwoIcons(icons:Vector.<Sprite>, callback:Function):void
		{	
			var targetSideDim:Point = new Point(ms_TargetArea.width / 2 - (( 1 - ms_PERCENT_AVAILABLE_SPACE) * ms_TargetArea.width)/2 - ms_Spacer / 2, ms_TargetArea.height * ms_PERCENT_AVAILABLE_SPACE);
			var tweens:Vector.<TweenLite> = new Vector.<TweenLite>();
			var targetPoints:Vector.<Point> = new Vector.<Point>();
			var targetDimensions:Vector.<Point> = new Vector.<Point>();
			
			for(var i:int = 0; i < icons.length; i++)
			{
				targetDimensions.push(calculateScaledDimensions(new Point(targetSideDim.x, targetSideDim.y), icons[i]));
				if(i == 0)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 - targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height / 2 - targetDimensions[i].y / 2));
				}
				else if( i == 1)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height / 2 - targetDimensions[i].y / 2));
				}
				
				if(i == icons.length - 1)
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, paused:true, onComplete:callback, ease:ms_Ease}));
				}
				else
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, paused:true, ease:ms_Ease}));
				}
			
			}
			
			for each(var tween:TweenLite in tweens)
			{
				tween.play();
			}
		}
		
		protected static function moveToThreeIcons(icons:Vector.<Sprite>, callback:Function):void
		{
			var targetSideDim:Point = new Point(ms_TargetArea.width / 2 - ((1 - ms_PERCENT_AVAILABLE_SPACE) * ms_TargetArea.width)/2 - ms_Spacer / 2, 
				ms_TargetArea.height / 2 - ((1 - ms_PERCENT_AVAILABLE_SPACE) * ms_TargetArea.height)/2 - ms_Spacer / 2);
			var tweens:Vector.<TweenLite> = new Vector.<TweenLite>();
			var targetPoints:Vector.<Point> = new Vector.<Point>();
			var targetDimensions:Vector.<Point> = new Vector.<Point>();
			
			for(var i:int = 0; i < icons.length; i++)
			{
				targetDimensions.push(calculateScaledDimensions(new Point(targetSideDim.x, targetSideDim.y), icons[i]));
				if(i == 0)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 - targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 1)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 2)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 -targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height * 0.66 - targetDimensions[i].y / 2 + ms_Spacer / 2));
				}
				
				if(i == icons.length - 1)
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, paused:true, onComplete:callback, ease:ms_Ease}));
				}
				else
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, paused:true, ease:ms_Ease}));
				}	
			}
			
			for each(var tween:TweenLite in tweens)
			{
				tween.play();
			}
		}
		
		protected static function moveToFourIcons(icons:Vector.<Sprite>, callback:Function):void
		{
			var targetSideDim:Point = new Point(ms_TargetArea.width / 2 - ((1 - ms_PERCENT_AVAILABLE_SPACE) * ms_TargetArea.width)/2 - ms_Spacer / 2, 
				ms_TargetArea.height / 2 - ((1 - ms_PERCENT_AVAILABLE_SPACE) * ms_TargetArea.height)/2 - ms_Spacer / 2);
			var tweens:Vector.<TweenLite> = new Vector.<TweenLite>();
			var targetPoints:Vector.<Point> = new Vector.<Point>();
			var targetDimensions:Vector.<Point> = new Vector.<Point>();
			
			for(var i:int = 0; i < icons.length; i++)
			{
				targetDimensions.push(calculateScaledDimensions(new Point(targetSideDim.x, targetSideDim.y), icons[i]));
				if(i == 0)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 - targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 1)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 2)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 -targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height * (2/3) - targetDimensions[i].y / 2 + ms_Spacer / 2));
				}
				else if (i == 3)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height * (2/3) - targetDimensions[i].y / 2 + ms_Spacer / 2));
				}
				
				if(i == icons.length - 1)
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, ease:ms_Ease, paused:true, onComplete:callback}));
				}
				else
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, ease:ms_Ease,  paused:true}));
				}	
			}
			
			for each(var tween:TweenLite in tweens)
			{
				tween.play();
			}
		}
		
		protected static function moveToFiveIcons(icons:Vector.<Sprite>, callback:Function):void
		{	
			var targetSideDim:Point = new Point((ms_TargetArea.width * ms_PERCENT_AVAILABLE_SPACE) / 2 - ms_Spacer / 2, (ms_TargetArea.width * ms_PERCENT_AVAILABLE_SPACE) / 3 - ms_Spacer / 2);
			var tweens:Vector.<TweenLite> = new Vector.<TweenLite>();
			var targetPoints:Vector.<Point> = new Vector.<Point>();
			var targetDimensions:Vector.<Point> = new Vector.<Point>();
			
			for(var i:int = 0; i < icons.length; i++)
			{
				targetDimensions.push(calculateScaledDimensions(new Point(targetSideDim.x, targetSideDim.y), icons[i]));
				if(i == 0)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 - targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 1)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height / 3 - targetDimensions[i].y / 2 - ms_Spacer / 2));
				}
				else if( i == 2)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 3 -targetDimensions[i].x / 2 - ms_Spacer / 2, ms_TargetArea.height * (2/3) - targetDimensions[i].y / 2 + ms_Spacer / 2));
				}
				else if (i == 3)
				{
					targetPoints.push(new Point(ms_TargetArea.width * (2/3) -targetDimensions[i].x / 2 + ms_Spacer / 2, ms_TargetArea.height * (2/3) - targetDimensions[i].y / 2 + ms_Spacer / 2));
				}
				else if (i == 4)
				{
					targetPoints.push(new Point(ms_TargetArea.width / 2 - targetDimensions[i].x / 2, ms_TargetArea.height / 2 - targetDimensions[i].y / 2));
				}
				
				if(i == icons.length - 1)
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, ease:ms_Ease, paused:true, onComplete:callback}));
				}
				else
				{
					tweens.push(TweenLite.to(icons[i], ms_TWEEN_DURATION, {x:targetPoints[i].x, y:targetPoints[i].y, width:targetDimensions[i].x, height:targetDimensions[i].y, ease:ms_Ease, paused:true}));
				}
			}
			
			for each(var tween:TweenLite in tweens)
			{
				tween.play();
			}
		}
		
		protected static function calculateScaledDimensions(targetDims:Point, src:Sprite):Point
		{
			var targetWidth:Number;
			var targetHeight:Number;
			var icon:Sprite = src;
			
			if(icon.width > icon.height)
			{
				targetWidth = targetDims.x;
				targetHeight = icon.height * (targetWidth / icon.width);
				
				if(targetHeight > targetDims.y)
				{
					targetHeight = targetDims.y;
					targetWidth = icon.width * (targetHeight / icon.height);
				}
			}
			else
			{
				targetHeight = targetDims.y;
				targetWidth = icon.width * (targetHeight / icon.height);
				
				if(targetWidth > targetDims.x)
				{
					targetWidth = targetDims.x;
					targetHeight = icon.height * (targetWidth / icon.width);
				}
			}
			
			return new Point(targetWidth, targetHeight);
		}
	}
}
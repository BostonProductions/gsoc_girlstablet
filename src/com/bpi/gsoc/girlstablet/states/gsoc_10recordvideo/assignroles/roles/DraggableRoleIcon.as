package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.citrusas.ui.draggableobject.DraggableObject;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.events.Event_RoleIconDropped;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	
	public class DraggableRoleIcon extends DraggableObject
	{	
		public static const DraggableRoleEventDispatcher:EventDispatcher = new EventDispatcher();
		private var m_RoleName:String;
		
		public function DraggableRoleIcon(draggableObject:Sprite, name:String)
		{
			super(draggableObject);
			m_RoleName = name;
		}
		
		public override function AddEventListeners():void
		{
			super.AddEventListeners();
			
			CitrusFramework.frameworkStage.addEventListener(Event.ENTER_FRAME, eh_Update);
		}
		
		public override function RemoveEventListeners():void
		{
			super.RemoveEventListeners();
			
			CitrusFramework.frameworkStage.removeEventListener(Event.ENTER_FRAME, eh_Update);
		}
		
		protected function eh_Update(evt:Event):void
		{
			var isRecordStateControllerActive:Boolean = StateControllerManager.IsStateControllerActive(StateController_IDs.STATECONTROLLER_RECORDVIDEO);
			var isAssignRolesStateActive:Boolean = StateControllerManager.GetActiveAppStateID(StateController_IDs.STATECONTROLLER_RECORDVIDEO) == StateController_StateIDs_RecordVideo.STATE_ASSIGNROLES;
			if(!isRecordStateControllerActive || !isAssignRolesStateActive)
			{
				Destroy();
			}
		}
		
		override protected function eh_StopDrag(e:Event):void
		{
			if(e is MouseEvent || (e is TouchEvent && (e as TouchEvent).touchPointID == m_TouchID))
			{
				this.m_DraggableObject.stage.removeEventListener(InputController.UP, eh_StopDrag);
				this.m_DraggableObject.stage.removeEventListener(InputController.MOVE, eh_UpdateShapePos);
				DraggableRoleEventDispatcher.dispatchEvent(new Event_RoleIconDropped(Event_RoleIconDropped.FREE_ROLE_ICON_DROPPED,  new Point(this.m_DraggableObject.x, this.m_DraggableObject.y), this));
			}
		}
		
		override public function get draggableObject():DisplayObject
		{
			return this.m_DraggableObject as Sprite;
		}
		
		public function get roleName():String
		{
			return m_RoleName;
		}
		
		public function Destroy():void
		{
			if(this.draggableObject.parent)
			{
				this.draggableObject.parent.removeChild(this.draggableObject);
			}
			m_RoleName = null;
			this.RemoveEventListeners();
		}
		
		public function FlyToStartAndDisappear():void
		{
			this.RemoveEventListeners();
			var tl:TimelineLite = new TimelineLite();
			tl.add(TweenLite.to(this.draggableObject, 0.4, {x:this.dragStartPosition.x, y:this.dragStartPosition.y}));
			tl.add(TweenLite.to(this.draggableObject, 0.15, {alpha:0, onComplete: this.Destroy}));
			tl.play();
		}
	}
}
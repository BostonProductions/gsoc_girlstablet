package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.teamdisplay
{
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.target.TargetBox;
	
	import flash.display.Sprite;
	
	public class SelectedRolesDisplay extends Sprite
	{
		private var m_Items:Vector.<TargetBox>;
		
		public function SelectedRolesDisplay(numBuckets:int, bucketBackground:Class)
		{
			m_Items = new Vector.<TargetBox>();
			for(var i:int = 0; i < numBuckets; i++)
			{
				var newBox:TargetBox = new TargetBox(new bucketBackground());
				newBox.scaleX = newBox.scaleY = 1.15;  // Late-in-the-game request to make target boxes larger. I suspect that this will go under several iterations,
				// so I'm altering it here instead of in graphics.
				m_Items.push(newBox);
				this.addChild(newBox);
			}
		}
		
		public function GetRoleBoxAt(i:int):TargetBox
		{
			return m_Items[i];
		}
		
		public function GetNumRoles():int
		{
			return m_Items.length;
		}
		
		public function DeInit():void
		{
			for each (var targetBox:TargetBox in m_Items)
			{
				this.removeChild(targetBox);
				targetBox.DeInit();
				m_Items = null;
			}
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	import com.bpi.citrusas.logging.LocalLogger;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.videoupload.VideoUpload_MessageBuilder;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.events.Event_FFMPEGProgress;
	
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.EventDispatcher;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;

	public class FFMPEGExporter extends EventDispatcher
	{
		private static var ms_FfmpegFile:File;
		private static var ms_NativeProcess:NativeProcess;
		private static var ms_StartupInfo:NativeProcessStartupInfo;
		private static var ms_Logger:LocalLogger;
		private static var ms_LoggingFilePath:String;
		private static var ms_ProgressMatch:RegExp;
		private static var ms_FrameNumberMatch:RegExp;
		private static var ms_MinimumEncodingUpdateDuration:uint = 1000; // in milliseconds
		private static var ms_LastEncodingUpdateTime:uint; 
		
		public static const EventDispatcher_FFMPEGProgress:EventDispatcher = new EventDispatcher();
		
		public static function Initialize(ffmpeg:File):void
		{
			ms_LoggingFilePath = File.applicationDirectory.nativePath + "\\videoExportLog.txt";
			var log:File = new File(ms_LoggingFilePath);
			if(log.exists)
			{
				// clear it - we only keep the log of the most recent encoding
				var stream:FileStream = new FileStream();
				stream.open(log, FileMode.WRITE);
				stream.writeUTFBytes("");
				stream.close();
			}
			
			ms_Logger = new LocalLogger(ms_LoggingFilePath);
			
			ms_ProgressMatch = new RegExp("frame=\\s+\\d+");
			ms_FrameNumberMatch = new RegExp("\\d+");
			
			ms_LastEncodingUpdateTime = 0; // force an update on the first stderr output
			
			if(ffmpeg.exists)
			{
				ms_FfmpegFile = ffmpeg;
			}
			else
			{
				var msg:String = "FFMPEG directory is invalid. Directory received was: " + ffmpeg.nativePath;
				ms_Logger.LogEvent(msg);
				throw new Error(msg);
			}
		}
		
		public static function StartFFMPEG(ffmpegCall:FFMPEGCall, onComplete:Function, suppressOutput:Boolean=true):void
		{
			if(checkIfEncodingSupported())
			{
				ms_StartupInfo = new NativeProcessStartupInfo();
				ms_StartupInfo.executable = ms_FfmpegFile;
				ms_StartupInfo.workingDirectory = File.applicationDirectory.resolvePath(File.applicationDirectory.nativePath + "\\data\\ffmpeg");
				
				ms_StartupInfo.arguments = ffmpegCall.arguments;
				
				ms_NativeProcess = new NativeProcess();
				ms_NativeProcess.addEventListener(NativeProcessExitEvent.EXIT, onComplete);
				ms_NativeProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, function(e:ProgressEvent):void{onStandardErrorOutput(e, ffmpegCall.numFrames, ffmpegCall.callID)});
				ms_NativeProcess.start(ms_StartupInfo);
			}
		}
		
		private static function checkIfEncodingSupported():Boolean
		{
			if(NativeProcess.isSupported)
			{
				return true;
			}
			else
			{
				var msg:String = "Native process FFMPEG was not supported.";
				ms_Logger.LogEvent(msg);
				return false;
			}
		}
		
		private static function onStandardErrorOutput(e:ProgressEvent, numFrames:uint, progressID:String):void
		{
			var newData:String = ms_NativeProcess.standardError.readUTFBytes(ms_NativeProcess.standardError.bytesAvailable);
			ms_Logger.LogEvent(newData);
			var result:Object = ms_ProgressMatch.exec(newData);
			if(result)
			{
				var frameNumber:int;
				var digitResult:Object = ms_FrameNumberMatch.exec(result[0]);
				
				if(digitResult)
				{
					var now:uint = (new Date()).time;
					if(now - ms_LastEncodingUpdateTime > ms_MinimumEncodingUpdateDuration)
					{
						frameNumber = parseInt(digitResult[0], 10);
						
						var networkMessageByteArray:ByteArray = VideoUpload_MessageBuilder.BuildNetworkMessage(int(frameNumber/numFrames * 100), progressID, false, SessionInformation_GirlsTablet.TabletIndex);
						Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_UPDATE_VIDEOUPLOADPROGRESS, networkMessageByteArray));
						EventDispatcher_FFMPEGProgress.dispatchEvent(new Event_FFMPEGProgress(Event_FFMPEGProgress.ENCODING_PROGRESS, networkMessageByteArray));
						ms_LastEncodingUpdateTime = now;
					}					
					
				}	
			}
		}
	}
}
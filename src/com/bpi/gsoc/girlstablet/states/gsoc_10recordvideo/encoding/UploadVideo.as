package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding
{
	import com.bpi.citrusas.citrus.api.enums.BPINetContentTypes;
	import com.bpi.citrusas.citrus.controllers.user.UserDataMethods;
	import com.bpi.citrusas.citrus.controllers.user.events.Event_UserDataController;
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.citrusas.networking.client.Client;
	import com.bpi.citrusas.networking.events.Event_ClientNetworkMessage;
	import com.bpi.gsoc.framework.constants.SessionConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.videoupload.VideoUpload_MessageBuilder;
	import com.bpi.gsoc.framework.networking.GSOC_CustomUserDataMethods;
	import com.bpi.gsoc.framework.networking.NetworkMessage_Types;
	
	import flash.utils.ByteArray;

	public final class UploadVideo
	{
		private static const EVENT_UPLOADVIDEO_LOGIN:String = "EVENT_UPLOADVIDEO_LOGIN";
		private static const EVENT_UPLOADVIDEO_UPLOAD:String = "EVENT_UPLOADVIDEO_UPLOAD";
		
		private static var m_AuthorizationToken:String;
		private static const UPLOADING:String = "Uploading";
		private static const UPLOAD_COMPLETE:String = "Uploaded";
		public static const UPLOAD_FAILED:String = "Failed";
		public static var currentTitle:String = "";
		
		private static var ms_MinimumUploadUpdateDuration:uint = 1000; // in milliseconds
		private static var ms_LastUploadUpdateTime:Number;
		
		public static function beginVideoUpload():void
		{
			ms_LastUploadUpdateTime = 0; // force an update on the first frame
			login();
		}
		
		private static function login():void
		{
			addLoginEventListeners();
			UserDataMethods.GetUserAuthorizationToken(CitrusFramework.serverURL, SessionInformation_GirlsTablet.Authentication_Leader_UserName, SessionInformation_GirlsTablet.Authentication_Leader_Password, EVENT_UPLOADVIDEO_LOGIN);
		}
		
		private static function eh_LoginComplete(event:Event_UserDataController):void
		{
			if(event.eventKey == EVENT_UPLOADVIDEO_LOGIN)
			{
				removeLoginEventListeners();
				
				var username:String = event.eventData["username"];
				m_AuthorizationToken = event.eventData["returnedData"]["token"];
				
				uploadVideo();
			}
		}
		
		private static function eh_LoginFailed(event:Event_UserDataController):void
		{
			if(event.eventKey == EVENT_UPLOADVIDEO_LOGIN)
			{
				removeLoginEventListeners();
			}
		}
		
		private static function addLoginEventListeners():void
		{
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
			UserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
		}
		
		private static function removeLoginEventListeners():void
		{
			UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_COMPLETE, eh_LoginComplete);
			UserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_TOKEN_RETRIEVED_ERROR, eh_LoginFailed);
		}
		
		private static function uploadVideo():void
		{
			addUploadFilePathEventListeners();
			var videoFilePath:String = CitrusFramework.frameworkConfigXML.videoEncodingConfigs.videoProcessingDirectory.toString() + CitrusFramework.frameworkConfigXML.videoEncodingConfigs.encodedVideoPath.toString();
			var teamName:String = "unknown";
			switch(SessionInformation_GirlsTablet.TabletIndex)
			{
				case 0:
					teamName = "purple";
					break;
				case 1:
					teamName = "turquoise";
					break;
				case 2: 
					teamName = "blue";
					break;
				case 3:
					teamName = "yellow";
					break;
			}
			
			var metadata:Object = new Object();
			metadata["team_color"] = teamName;
			GSOC_CustomUserDataMethods.UploadFilePath(CitrusFramework.serverURL, m_AuthorizationToken, SessionInformation_GirlsTablet.Authentication_SessionID, videoFilePath, SessionInformation_GirlsTablet.TeamInfo.Tablet_ID + "/" + SessionConstants.TEAM_VIDEO, BPINetContentTypes.VIDEO_MP4, metadata, currentTitle, EVENT_UPLOADVIDEO_UPLOAD);
		}
		
		private static function eh_VideoUploaded(event:Event_UserDataController):void
		{
			if(event.eventKey == EVENT_UPLOADVIDEO_UPLOAD)
			{
				removeUploadFilePathEventListeners();
				sendNetworkMessage(100, UPLOAD_COMPLETE, true);
			}
		}
		
		private static function eh_VideoUploadFailed(event:Event_UserDataController):void
		{
			if(event.eventKey == EVENT_UPLOADVIDEO_UPLOAD)
			{
				removeUploadFilePathEventListeners();
				trace("Video could not be uploaded");
				sendNetworkMessage(100, UPLOAD_FAILED, true);
			}
		}
		
		private static function eh_VideoProgress(event:Event_UserDataController):void
		{
			if(event.eventKey == EVENT_UPLOADVIDEO_UPLOAD)
			{
				sendNetworkMessage((event.eventData.bytesLoaded / event.eventData.bytesTotal) * 100, UPLOADING, false);
			}
		}
		
		private static function addUploadFilePathEventListeners():void
		{
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_UPLOAD_COMPLETE, eh_VideoUploaded);
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_UPLOAD_ERROR, eh_VideoUploadFailed);
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.addEventListener(Event_UserDataController.USER_FILE_UPLOAD_PROGRESS, eh_VideoProgress);
		}
		
		private static function removeUploadFilePathEventListeners():void
		{
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_UPLOAD_COMPLETE, eh_VideoUploaded);
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_UPLOAD_ERROR, eh_VideoUploadFailed);
			GSOC_CustomUserDataMethods.userDataMethodEventDispatcher.removeEventListener(Event_UserDataController.USER_FILE_UPLOAD_PROGRESS, eh_VideoProgress);
		}
		
		private static function sendNetworkMessage(percentComplete:int, label:String, isUploaded:Boolean):void
		{
			var now:Number = (new Date()).time;
			if(now - ms_LastUploadUpdateTime > ms_MinimumUploadUpdateDuration || isUploaded)
			{
				trace("Sending uploading update");
				var networkMessageByteArray:ByteArray = VideoUpload_MessageBuilder.BuildNetworkMessage(percentComplete, label, isUploaded, SessionInformation_GirlsTablet.TabletIndex);
				Client.ClientEventDispatcher.dispatchEvent(new Event_ClientNetworkMessage(Event_ClientNetworkMessage.EVENT_SEND_NETWORK_MESSAGE, NetworkMessage_Types.TELL_SERVER_UPDATE_VIDEOUPLOADPROGRESS, networkMessageByteArray));
				ms_LastUploadUpdateTime = now;
			}
		}
	}
}
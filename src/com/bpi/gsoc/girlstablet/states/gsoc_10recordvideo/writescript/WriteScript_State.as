package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript
{
	import com.bpi.citrusas.citrus.framework.CitrusFramework;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.states.app.GSOC_AppState;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	public class WriteScript_State extends GSOC_AppState
	{
		private var m_App:WriteScript_App;
		
		public function WriteScript_State()
		{
			super(StateController_StateIDs_RecordVideo.STATE_WRITESCRIPT);
		}
		
		public override function Initialize():void
		{
			
		}
		
		protected override function Init():void
		{
			switch(SessionInformation_GirlsTablet.Age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					m_App = new WriteScript_App_DB();
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					m_App = new WriteScript_App_JC();
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					m_App = new WriteScript_App_SA();
					break;
			}
			
			m_App.Initialize();
			CitrusFramework.contentLayer.addChild(m_App);
			
			m_App.Init();
		}
	
		protected override function DeInit():void
		{	
			CitrusFramework.contentLayer.removeChild(m_App);
			m_App = null;
			
			super.EndDeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript
{
	import com.bpi.gsoc.components.recordvideo.WriteScript_SA_Skin;
	import com.bpi.gsoc.framework.constants.TrackConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_Header;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.writescript.classes.StaggeredAnimation;

	public class WriteScript_App_SA extends WriteScript_App
	{
		public function WriteScript_App_SA(addPagination:Boolean=false)
		{
			super(addPagination);
		}
		
		public override function Initialize():void
		{
			m_Skin = new WriteScript_SA_Skin();
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_Skin.scriptContent.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			m_NextButton = m_Skin.nextBtn;
			var rcWidth:int = SessionInformation_GirlsTablet.Track == TrackConstants.CAREER ? 1571 : 1500;
			m_RoleCallout = new RoleCallout(m_Skin.header.paginationArea.x, m_Skin.header.paginationArea.y, m_Skin.header.paginationArea.width, m_Skin.header.paginationArea.height);
			m_Instructions = new StaggeredAnimation(m_Skin.scriptContent);
			this.addChild(m_Skin);
			
			m_GSOCHeader = new GSOC_Header(m_Skin.header.paginationArea);
			GSOCheader.addArrows(m_Skin.header.paginationArea.x, 0);
			super.Initialize();
		}
	}
}
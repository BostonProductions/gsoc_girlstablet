package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.filterscreen
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Image;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.components.recordvideo.SA_FilterSelection;
	import com.bpi.gsoc.framework.constants.GSOC_Info;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.data.userdata.GSOC_UserData_Team_DataKeys;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.FilterKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement.EditVideoStateIDs;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	public class FilterScreen extends EditScreen
	{
		private var m_TempImage:Sprite;
		private var m_Loader:Loader;
		private var m_TypedSkin:SA_FilterSelection;
		private var m_SelectionMade:Boolean;
		
		public function FilterScreen()
		{
			super(EditVideoStateIDs.STATE_FILTER);
		}
		
		public override function Init():void
		{
			m_SelectableItems = new Vector.<MovieClip>();
			m_Skin = new SA_FilterSelection();
			m_TypedSkin = m_Skin as SA_FilterSelection;
			m_TempImage = new Sprite();
			m_GlowColor = GSOC_Info.ColorTwo;
			m_SelectionMade = false;
						
			m_Loader = new Loader();
			m_Loader.contentLoaderInfo.addEventListener(Event.COMPLETE, eh_CreateFilterPreviewButtons);
			m_Loader.load(new URLRequest("data/filterthumbnailimage.jpg"));
			
			m_TypedSkin.noneBtn.addEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			
			super.Init();
		}
		
		private function eh_CreateFilterPreviewButtons(e:Event):void
		{
			m_Loader.removeEventListener(Event.COMPLETE, eh_CreateFilterPreviewButtons);
			var image:Bitmap = Bitmap(m_Loader.content);
			var imgContainer:Sprite = new Sprite();
			imgContainer.addChild(image);
			
			this.addButton(m_TypedSkin.filterDisplay1, imgContainer, m_TypedSkin.filterLabel1.label, FilterKeys.BLACK_AND_WHITE);
			this.addButton(m_TypedSkin.filterDisplay2, imgContainer, m_TypedSkin.filterLabel2.label, FilterKeys.VINTAGE);
			this.addButton(m_TypedSkin.filterDisplay3, imgContainer, m_TypedSkin.filterLabel3.label, FilterKeys.SATURATED);
			this.addButton(m_TypedSkin.filterDisplay4, imgContainer, m_TypedSkin.filterLabel4.label, FilterKeys.ON_FIRE);
			this.addButton(m_TypedSkin.filterDisplay5, imgContainer, m_TypedSkin.filterLabel5.label, FilterKeys.UNDER_THE_SEA);
			
			this.clearSelections();
			
			for each(var btn:FilterButton in m_SelectableItems)
			{
				this.addChild(btn);
				btn.addEventListener(InputController.CLICK, this.eh_OptionSelected);
				var dropShadowFilter:DropShadowFilter = new DropShadowFilter(10, 45, 0, 0.5, 35, 35, 1);
				btn.filters = [dropShadowFilter];
			}
		}
		
		private function addButton(mc:MovieClip, imgContainer:Sprite, label:TextField, filterKey:String):void
		{
			m_SelectableItems.push(new FilterButton(mc, Utils_Image.CopySprite(imgContainer), filterKey));
			Utils_Text.ConvertToEmbeddedFont(label, GSOC_Info.BodyFont);
			label.text = filterKey;
			label.height = 200; // Could have done this in the XFL, I guess. Ensures that long titles don't get cut off.
			label.wordWrap = true;
			label.selectable = false;
		}
		
		private function detectInteractionOnce():void
		{
			var priorSelectionMade:Boolean = m_SelectionMade;
			m_SelectionMade = true;
			if(!priorSelectionMade) super.onScreenComplete();
		}
		
		override protected function eh_OptionSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTFILTER);
			this.detectInteractionOnce();
			super.eh_OptionSelected(e);
			for each(var option:FilterButton in m_SelectableItems)
			{
				option.GoToUnhighlightedState();
			}
			(e.currentTarget as FilterButton).GoToHighlightedState();
			
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER, (e.currentTarget as FilterButton).filterKey);
		}
		
		override protected function eh_NoneButtonSelected(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SELECTFILTER);
			SessionInformation_GirlsTablet.TeamInfo.Team_UserData.SetData(GSOC_UserData_Team_DataKeys.KEY_VIDEO_FILTER, null);
			for each(var option:FilterButton in m_SelectableItems)
			{
				option.GoToUnhighlightedState();
			}
			this.detectInteractionOnce();
			super.eh_NoneButtonSelected(e);
		}
		
		public override function DeInit():void
		{
			for each(var btn:FilterButton in m_SelectableItems)
			{
				btn.removeEventListener(InputController.CLICK, this.eh_OptionSelected);
			}
			m_TypedSkin.noneBtn.removeEventListener(InputController.CLICK, this.eh_NoneButtonSelected);
			super.DeInit();
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.songscreen.events
{
	import flash.events.Event;
	
	public class Event_SoundLoaded extends Event
	{
		private var m_SoundName:String;
		
		public function Event_SoundLoaded(type:String, soundName:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			m_SoundName = soundName;
		}
		
		public function get soundName():String
		{
			return m_SoundName;
		}
	}
}
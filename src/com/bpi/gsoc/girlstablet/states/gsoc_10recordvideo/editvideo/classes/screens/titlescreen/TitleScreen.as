package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.gsoc.framework.components.NoTitleImage;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.titlescreen.fonts.EmbeddedVideoFonts;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement.EditVideoStateIDs;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;

	public class TitleScreen extends EditScreen
	{
		protected var m_NoTitleImage:NoTitleImage;
	
		public function TitleScreen()
		{
			super(EditVideoStateIDs.STATE_TITLE);
			EmbeddedVideoFonts.InitializeFonts();
		}
		
		public override function Init():void
		{
			m_NoTitleImage = new NoTitleImage();

			for each(var option:MovieClip in m_SelectableItems)
			{
				option.addEventListener(InputController.CLICK, eh_OptionSelected);
				TweenLite.to(option, 0.3, {glowFilter:{color:m_GlowColor, blurX:0, blurY:0, strength:1, alpha:0}});
			}
			
			m_Skin.noneBtn.visible = false;
			this.unhighlightNoneButton();
			
			super.Init();
		}
		
		public override function DeInit():void
		{
			for each(var option:MovieClip in m_SelectableItems)
			{
				option.removeEventListener(InputController.CLICK, eh_OptionSelected);
				option.filters = [];
			}
			m_Skin.noneBtn.removeEventListener(InputController.CLICK, eh_NoneButtonSelected);
			super.DeInit();
		}
		
		public function get skin():MovieClip
		{
			return m_Skin;
		}
	}
}
package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters
{
	public class Filter_Vintage extends Filter
	{
		public function Filter_Vintage()
		{
			super();
			
			m_Matrix = new Array();
			m_Matrix = m_Matrix.concat([0.393,0.769,0.189,0,0]);// red
			m_Matrix = m_Matrix.concat([0.349,0.686,0.168,0,0]);// green
			m_Matrix = m_Matrix.concat([0.272,0.534,0.131,0,0]);// blue
			m_Matrix = m_Matrix.concat([0,0,0,1,0]);// alpha
		}
	}
}
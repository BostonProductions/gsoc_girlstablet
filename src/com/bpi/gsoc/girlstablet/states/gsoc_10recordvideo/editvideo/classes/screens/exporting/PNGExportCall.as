package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting
{
	import flash.display.MovieClip;

	public class PNGExportCall
	{
		private var m_Source:MovieClip;
		private var m_TargetPath:String;
		
		public function PNGExportCall(src:MovieClip, target:String)
		{
			m_Source = src;
			m_TargetPath = target;
		}
		
		public function get source():MovieClip
		{
			return m_Source;
		}
		
		public function get targetPath():String
		{
			return m_TargetPath;
		}
	}
}
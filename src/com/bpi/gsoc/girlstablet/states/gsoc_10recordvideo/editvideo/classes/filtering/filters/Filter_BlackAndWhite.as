package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.filtering.filters
{
	public class Filter_BlackAndWhite extends Filter
	{
		public function Filter_BlackAndWhite()
		{
			super();
			
			m_Matrix = new Array();
			m_Matrix = m_Matrix.concat([0.5,0.5,0.5,0,0]);// red
			m_Matrix = m_Matrix.concat([0.5,0.5,0.5,0,0]);// green
			m_Matrix = m_Matrix.concat([0.5,0.5,0.5,0,0]);// blue
			m_Matrix = m_Matrix.concat([0,0,0,1,0]);// alpha
		}
	}
}
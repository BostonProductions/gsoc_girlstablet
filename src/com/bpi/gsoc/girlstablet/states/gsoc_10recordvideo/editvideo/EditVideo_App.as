package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.states.StateControllerManager;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.gsocui.GSOC_ButtonToggle;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	import com.bpi.gsoc.framework.states.app.GSOC_AppSkin;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.RoleCallout;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.assignroles.roles.RoleKeys;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.EditScreen;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.screens.exporting.Queue_PNGExport;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.statemanagement.EditScreenStateController;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.EncodingWrapper;
	import com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.encoding.Queue_FFMPEGCalls;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_IDs;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Main;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_Poster;
	import com.bpi.gsoc.girlstablet.states.stateids.StateController_StateIDs_RecordVideo;
	
	import flash.events.Event;
	
	public class EditVideo_App extends GSOC_AppSkin
	{
		public var currentIndex:int;
		
		protected var m_EditingStateController:EditScreenStateController;
		protected var m_OkToBeginVidComposite:Boolean;
		protected var m_RoleCallout:RoleCallout;
		
		public function EditVideo_App(addPagination:Boolean=false)
		{
			super(this);
			m_EditingStateController = new EditScreenStateController();
		}
		
		public override function Init():void
		{
			m_EditingStateController.GoToFirstState();
			this.addChildAt(m_EditingStateController.GetCurrentEditScreen(), 1);
			m_EditingStateController.GetCurrentEditScreen().addEventListener(EditScreen.SCREEN_COMPLETE, this.eh_OnScreenComplete);
			
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.submit_Button, null, null);
			m_Skin.header.gotoAndStop(SessionInformation_GirlsTablet.Track + 1);
			
			
			m_GSOCHeader.arrows.animateArrows();
			
			Queue_PNGExport.Initialize();
			Queue_PNGExport.QueueEventDispatcher.addEventListener(Queue_PNGExport.PNG_QUEUE_COMPLETE, this.eh_OnPNGExportComplete);
			
			m_OkToBeginVidComposite = false;
			
			m_RoleCallout.FillForRole(RoleKeys.EDITOR);
			m_RoleCallout.AnimateIn();
			
			this.addChild(m_RoleCallout);
			
			super.Init();
		}
		
		public override function DeInit():void
		{
			super.DeInit();
		}
		
		private function enableSubmit(e:Event = null):void
		{
			m_Skin.submit_Button.addEventListener(InputController.CLICK, this.goToNextScreen);
		}
		
		private function disableSubmit(e:Event = null):void
		{
			m_Skin.submit_Button.removeEventListener(InputController.CLICK, this.goToNextScreen);
		}
		
		private function eh_OnScreenComplete(e:Event):void
		{
			GSOC_ButtonToggle.ToggleButton(true, m_Skin.submit_Button, this.enableSubmit, null); 
		}
		
		private function eh_OnScreenIncomplete(e:Event):void
		{
			GSOC_ButtonToggle.ToggleButton(false, m_Skin.submit_Button, null, this.disableSubmit);
		}
		
		private function eh_OnPNGExportComplete(e:Event):void
		{
			m_OkToBeginVidComposite = true
			Queue_PNGExport.QueueEventDispatcher.removeEventListener(Queue_PNGExport.PNG_QUEUE_COMPLETE, eh_OnPNGExportComplete);
			this.beginVideoComposite();
		}
		
		private function goToNextScreen(e:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			var screenToDismiss:EditScreen = m_EditingStateController.GetCurrentEditScreen();
			screenToDismiss.removeEventListener(EditScreen.SCREEN_COMPLETE, this.goToNextScreen);
			m_EditingStateController.GetCurrentEditScreen().removeEventListener(EditScreen.SCREEN_INCOMPLETE, this.eh_OnScreenIncomplete);

			this.removeChild(screenToDismiss);
			
			if(m_EditingStateController.HasNextState())
			{
				m_EditingStateController.GoToNextState();
				this.addChildAt(m_EditingStateController.GetCurrentEditScreen(), 1);
				m_EditingStateController.GetCurrentEditScreen().addEventListener(EditScreen.SCREEN_COMPLETE, this.eh_OnScreenComplete);
				m_EditingStateController.GetCurrentEditScreen().addEventListener(EditScreen.SCREEN_INCOMPLETE, this.eh_OnScreenIncomplete);
				GSOC_ButtonToggle.ToggleButton(false, m_Skin.submit_Button, null, this.disableSubmit); 
			}
			else
			{
				this.createFFMPEGCalls();
				screenToDismiss.DeInit();	
				
				if(SessionInformation_GirlsTablet.IsRunningLocally)
				{
					StateControllerManager.LeaveActiveState(StateController_IDs.STATECONTROLLER_RECORDVIDEO, StateController_StateIDs_RecordVideo.STATE_EDITVIDEO);
					StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_POSTER, StateController_StateIDs_Poster.STATE_POSTER);
				}
				else
				{
					submitActivity();
				}
				SessionInformation_GirlsTablet.UploadCurrentInformation();
			}
		}
		
		private function submitActivity():void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			StateControllerManager.LeaveAllActiveStates();
			StateControllerManager.MoveToState(StateController_IDs.STATECONTROLLER_MAIN, StateController_StateIDs_Main.STATE_SESSIONATTRACT);
			Document.TellServerActivityComplete();
		}
		
		private function createFFMPEGCalls():void
		{
			Queue_FFMPEGCalls.ClearQueue();
			
			Queue_FFMPEGCalls.Initialize();
			
			EncodingWrapper.CreateEncodingBatchCalls();
		}
		
		private function beginVideoComposite():void
		{
			Queue_FFMPEGCalls.StartQueue(); 
		}
	}
}
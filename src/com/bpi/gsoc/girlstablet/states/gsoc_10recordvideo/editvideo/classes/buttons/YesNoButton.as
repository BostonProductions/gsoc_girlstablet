package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.editvideo.classes.buttons
{
	import flash.display.MovieClip;
	import flash.media.Sound;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class YesNoButton extends MovieClip
	{
		public var music:Sound;
		private var titleText:TextField;
		private var selectedColor:uint = 0xff4511;
		private var unselectedColor:uint = 0x000000;
		
		public function YesNoButton(s:String, fl:String)
		{
			this.graphics.beginFill(0x33ccff);
			this.graphics.drawRect(0, 0, 200, 100);
			this.graphics.endFill();
			
			titleText = new TextField();
			titleText.width = 200;
			titleText.height = 100;
			titleText.x = 0;
			titleText.y = 0;
			titleText.border = true;
			titleText.borderColor = unselectedColor;
			titleText.wordWrap = true;
			titleText.embedFonts = true;
			titleText.selectable = false;
			titleText.mouseEnabled = false;
			
			var tf1SA:TextFormat = new TextFormat(fl, 24, 0x0044ff, false);
			
			titleText.defaultTextFormat = tf1SA;
			titleText.setTextFormat(tf1SA);
			
			titleText.text = s;
			this.addChild(titleText);	
		}
		
		public function setColor(isSelected:Boolean):void
		{
			if (isSelected)
			{
				titleText.borderColor = selectedColor;
			}
			else
			{
				titleText.borderColor = unselectedColor;
			}
		}
	}
}
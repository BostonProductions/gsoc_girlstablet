package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets
{
	import com.bpi.gsoc.girlstablet.global_data.TabletCamera;
	
	import flash.display.MovieClip;
	
	public class VideoBorder_RoundRect extends MovieClip
	{
		public function VideoBorder_RoundRect(color:uint)
		{
			super();
			
			this.graphics.lineStyle(VideoConstants.VIDEO_BORDER_WIDTH,color);
			this.graphics.beginFill(0xffffff, 0);
			this.graphics.drawRoundRect(0, 0, TabletCamera.REAR_CAMERA_DIMS.x, TabletCamera.REAR_CAMERA_DIMS.y, VideoConstants.VIDEO_ROUNDED_EDGE_RADIUS, VideoConstants.VIDEO_ROUNDED_EDGE_RADIUS);
			this.graphics.endFill();
		}
	}
}
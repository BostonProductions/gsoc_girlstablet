package com.bpi.gsoc.girlstablet.states.gsoc_10recordvideo.commonassets
{
	import flash.geom.Point;

	public class VideoConstants
	{
		public static const VIDEO_ROUNDED_EDGE_RADIUS:Number = 200;
		public static const VIDEO_BORDER_WIDTH:Number = 19;
		public static const VIDEO_FRAMERATE:Number = 30;
		public static const VIDEO_DIMS:Point = new Point(960, 540); //720x405
		
	}
}
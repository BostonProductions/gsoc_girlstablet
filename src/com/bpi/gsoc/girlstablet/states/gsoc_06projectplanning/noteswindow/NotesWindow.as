package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.noteswindow
{
	import com.bpi.citrusas.input.inputcontroller.InputController;
	import com.bpi.citrusas.sound.SoundHandler;
	import com.bpi.citrusas.utils.Utils_Text;
	import com.bpi.gsoc.framework.constants.AgeColors;
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.AgeFontLinkages;
	import com.bpi.gsoc.framework.constants.SessionConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.bpi.gsoc.framework.sfx.GSOC_SFX;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class NotesWindow extends EventDispatcher
	{
		private const TEXT:String = "text";
		private const TITLE:String = "title";
		private const CLOSE:String = "button_xclose";
		private const CLEAR:String = "button_clear";
		private const SAVE:String = "button_save";
		private const HITBOX:String = "notesHitBox";
		
		private var _notesWindow:MovieClip;
		
		public function NotesWindow(notesWindow:MovieClip, textfieldFormat:TextFormat, age:int)
		{
			this._notesWindow = notesWindow;
			
			switch(age)
			{
				case AgeConstants.KEY_DAISYBROWNIE:
					(this._notesWindow[TEXT] as TextField).maxChars = 289; // max 17 chars across x 17 lines down
					(this._notesWindow[TITLE] as TextField).defaultTextFormat = new TextFormat(AgeFontLinkages.DB_HEADER_FONT, 60, 0x993092, true);
					(this._notesWindow[TITLE] as TextField).setTextFormat(new TextFormat(AgeFontLinkages.DB_HEADER_FONT, 60, 0x993092, true));
					break;
				case AgeConstants.KEY_JUNIORCADETTE:
					(this._notesWindow[TEXT] as TextField).maxChars = 289; // max 17 chars across x 17 lines down
					(this._notesWindow[TITLE] as TextField).defaultTextFormat = new TextFormat(AgeFontLinkages.JC_HEADER_FONT, 60, 0x993092, true);
					(this._notesWindow[TITLE] as TextField).setTextFormat(new TextFormat(AgeFontLinkages.JC_HEADER_FONT, 60, 0x993092, true));
					break;
				case AgeConstants.KEY_SENIORAMBASSADOR:
					(this._notesWindow[TEXT] as TextField).maxChars = 255; // max 17 chars across x 15 lines down
					(this._notesWindow[TITLE] as TextField).defaultTextFormat = new TextFormat(AgeFontLinkages.SA_HEADER_FONT, 60, AgeColors.SA_YELLOW, true);
					(this._notesWindow[TITLE] as TextField).setTextFormat(new TextFormat(AgeFontLinkages.SA_HEADER_FONT, 60, AgeColors.SA_YELLOW, true));
					break;
			}
			(this._notesWindow[TEXT] as TextField).defaultTextFormat = textfieldFormat;
			(this._notesWindow[TEXT] as TextField).setTextFormat(textfieldFormat);
			
			(this._notesWindow[TITLE] as TextField).mouseEnabled = false;
			(this._notesWindow[TITLE] as TextField).multiline = true;
		}
		
		public function get notesWindow():MovieClip
		{
			return this._notesWindow;
		}
		
		public function get text():String
		{
			return (this._notesWindow[TEXT] as TextField).text;
		}
		
		public function AddEventListeners():void
		{
			(this._notesWindow[TEXT] as TextField).addEventListener(InputController.CLICK, this.TextClickHandler);
			(this._notesWindow[CLOSE] as MovieClip).addEventListener(InputController.CLICK, this.CloseClickHandler);
			(this._notesWindow[CLEAR] as MovieClip).addEventListener(InputController.CLICK, this.ClearClickHandler);
			(this._notesWindow[SAVE] as MovieClip).addEventListener(InputController.CLICK, this.SaveClickHandler);
			(this._notesWindow[HITBOX] as MovieClip).addEventListener(InputController.CLICK, this.ToggleNotesWindow);
		}
		
		public function RemoveEventListeners():void
		{
			(this._notesWindow[TEXT] as TextField).removeEventListener(InputController.CLICK, this.TextClickHandler);
			(this._notesWindow[CLOSE] as MovieClip).removeEventListener(InputController.CLICK, this.CloseClickHandler);
			(this._notesWindow[CLEAR] as MovieClip).removeEventListener(InputController.CLICK, this.ClearClickHandler);
			(this._notesWindow[SAVE] as MovieClip).removeEventListener(InputController.CLICK, this.SaveClickHandler);
			(this._notesWindow[HITBOX] as MovieClip).removeEventListener(InputController.CLICK, this.ToggleNotesWindow);
		}
		
		private function TextClickHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			(this._notesWindow[TEXT] as TextField).removeEventListener(InputController.CLICK, this.TextClickHandler);
			(this._notesWindow[TEXT] as TextField).text = "";
		}
		
		private function CloseClickHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLOSEBUTTONS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_NotesWindow(Event_NotesWindow.CLOSE));
		}
		
		private function ClearClickHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_CLICKS[SessionInformation_GirlsTablet.Age]);
			(this._notesWindow[TEXT] as TextField).removeEventListener(InputController.CLICK, this.TextClickHandler);
			(this._notesWindow[TEXT] as TextField).text = "";
			// refocus the text field
			(this._notesWindow[TEXT] as TextField).text = "";
			(this._notesWindow[TEXT] as TextField).stage.focus = (this._notesWindow[TEXT] as TextField);
		}
		
		private function SaveClickHandler(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_SAVEBUTTONS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_NotesWindow(Event_NotesWindow.SAVE));
		}
		
		private function ToggleNotesWindow(event:Event):void
		{
			SoundHandler.PlaySound(GSOC_SFX.SOUND_NOTESINOUTS[SessionInformation_GirlsTablet.Age]);
			this.dispatchEvent(new Event_NotesWindow(Event_NotesWindow.TOGGLE));
		}
		
		public function Init():void
		{
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_JUNIORCADETTE)
			{
				(this._notesWindow[TITLE] as TextField).text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.Hash;
			}
			else
			{
				(this._notesWindow[TITLE] as TextField).text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.Name;
				if((this._notesWindow[TITLE] as TextField).text.length == 0)
				{
					(this._notesWindow[TITLE] as TextField).text = SessionInformation_GirlsTablet.TeamInfo.Team_Issue.Text;
				}
			}
			Utils_Text.FitTextToTextfield(this._notesWindow[TITLE], 5);
			this._notesWindow[TITLE].y = this._notesWindow[TITLE].y + Math.round((this._notesWindow[TITLE].height - this._notesWindow[TITLE].textHeight) / 2);
		}
	}
}
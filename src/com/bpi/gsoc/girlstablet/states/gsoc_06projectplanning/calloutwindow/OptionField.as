package com.bpi.gsoc.girlstablet.states.gsoc_06projectplanning.calloutwindow
{
	import com.bpi.gsoc.framework.constants.AgeConstants;
	import com.bpi.gsoc.framework.constants.SessionConstants;
	import com.bpi.gsoc.framework.data.sessioninformation.SessionInformation_GirlsTablet;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	public class OptionField
	{
		private var m_Graphic:MovieClip;
		private var m_TextField:TextField;
		private static const BUFFER:Number = 60; // the number of pixels on the left and the right of the text field as it fits inside the background
		
		public function OptionField(graphic:MovieClip, textField:TextField)
		{
			m_Graphic = graphic;
			m_TextField = textField;
			
		}
		
		public function SetText(s:String):void
		{
			
			m_TextField.text = s;
			Refresh();
		}
		
		public function get background():MovieClip
		{
			return m_Graphic;
		}
		
		public function get textField():TextField
		{
			return m_TextField;
		}
		
		public function Refresh():void
		{
			var origGraphicPos:Point = new Point(m_Graphic.x, m_Graphic.y);
			var origTextPos:Point = new Point(m_TextField.x, m_TextField.y);
			var origWidth:Number = m_Graphic.width;

			m_Graphic.parent.visible = true;

			TweenLite.delayedCall(1, DelayedRefresh, [origGraphicPos, origTextPos, origWidth], true);
		}
		
		private function DelayedRefresh(origBackgroundPos:Point, origTextPos:Point, origBackgroundWidth:Number):void
		{
			// now that the text field is visible and we've waited a frame, we can get the accurate text width
			m_Graphic.width = m_TextField.textWidth + 2*BUFFER; 
			
			m_Graphic.parent.visible = false;
			
			var centeringReference:TextField = (m_Graphic.parent as MovieClip).questionText as TextField;
			m_TextField.width = m_Graphic.width;
			m_Graphic.x = centeringReference.x + centeringReference.width / 2 - m_Graphic.width / 2 + 5; // account for drop shadow;
			m_TextField.x = m_Graphic.x;
			if(SessionInformation_GirlsTablet.Age == AgeConstants.KEY_SENIORAMBASSADOR)
			{
				m_TextField.x -= 10; // DROPSHADOWS
			}

			// this is not actually a great fix. It depends on the text fields in the graphics having the appropriate
			// text already placed, in the correct font, and resized so that the text field width is not that much
			// wider than the text width. So the code doesn't do much but resize the background.
		}
	}
}